<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uu_id',
        'serial',
        'on_state',
        'city_id',
        'user',
        'age',
        'gender',
        'coordinates',
        'last_channel',
        'iptv_type'
    ];

    public function informativeVideos()
    {
        return $this->belongsToMany('App\InformativeVideo');
    }
}
