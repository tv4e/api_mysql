<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SendVideos' => [
            'App\Listeners\VideosListener',
        ],

        'App\Events\SendKey' => [
            'App\Listeners\KeyListener',
        ],

        'Laravel\Passport\Events\AccessTokenCreated' => [
            'App\Listeners\RevokeOldTokens',
        ],

        'Laravel\Passport\Events\RefreshTokenCreated' => [
            'App\Listeners\PruneOldTokens',
        ],

        'App\Events\SendHdmiState' => [
            'App\Listeners\HdmiListener',
        ],

         'App\Events\AppReady' => [
            'App\Listeners\AppReadyListener',
        ],

        'App\Events\RefreshLib' => [
            'App\Listeners\RefreshLibListener',
        ],

        'App\Events\RefreshContent' => [
            'App\Listeners\RefreshContentListener',
        ],

        'App\Events\RefreshChannels' => [
            'App\Listeners\RefreshChannelsListener',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
