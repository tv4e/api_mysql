<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationSourcesSub extends Model
{
    protected $table = 'information_sources_subs';

    public function relInformationSources()
    {
        return $this->belongsTo('App\InformationSource');
    }

    protected $fillable = [
        'url',
        'city_id',
        'information_source_id',
        'asgie_id',
        'carta_social_id',
    ];
}
