<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{

    public function cities()
    {
        return $this->hasMany('App\City', 'district_id')->select(array('district_id','name'));
    }

}
