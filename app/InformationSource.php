<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformationSource extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function relAsgie()
    {
        return $this->belongsTo('App\Asgie', 'asgie_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function relVideos()
    {
        return $this->hasMany('App\InformativeVideo');
    }

    public function relSubs()
    {
        return $this->hasMany('App\InformationSourcesSub');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'news_container',
        'news_link',
        'content_container',
        'content_title',
        'content_description',
        'asgie_id',
        'city_id',
        'html_exception'
    ];
}
