<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asgie extends Model
{
    protected $table = 'asgie';

    //

    /**
    * The roles that belong to the Video.
    */
    public function resources()
    {
        return $this->belongsToMany('App\AvResource', 'asgie_av_resource', 'asgie_id','av_resource_id')
            ->withPivot( "selected", "duration");
    }
}
