<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AppReady implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $box;

    /**
     * Create a new event instance.
     *
     * @param $box
     */
    public function __construct($box)
    {
        $this->box = $box;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('box.'.$this->box->serial);
    }
}