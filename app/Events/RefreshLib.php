<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RefreshLib implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $box;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($box)
    {
        $this->box = $box;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('box.'.$this->box->serial);
    }
}