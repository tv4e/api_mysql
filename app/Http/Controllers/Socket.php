<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\AppReady;
use App\Events\SendHdmiState;
use App\Box;
use App\Log;

class Socket extends Controller
{
    /**
     *
     * Used to send socket requests
     *
     * "AppReady" lets server know the app is ready to start
     *
     * "Disconnected" lets the server know the socket connection was lost

     * "Connected" lets the server know the socket connection was re-established
     *
     * "SendHdmiState" currently not being used
     *
     */
    public function emit(Request $request, $event, $serial)
    {   
        $obj = new \stdClass();
        $obj->serial = $serial;

        if($event == 'AppReady'){
            $obj->on_state = $request['on_state'];
            event(new AppReady($obj));
        } else if($event == 'Disconnected') {
            // LOG FILE ON STATE
            $state = Box::where('serial', $serial)->first();
            
            $log = new Log;
            $log->on_state = 0;
            $log->box_id = $state->id;
            $log->save();

            $state->on_state = 0;
            $state-> save();
        } else if($event == 'Connected') {
            // LOG FILE ON STATE
            $state = Box::where('serial', $serial)->first();

            $log = new Log;
            $log->on_state = 1;
            $log->box_id = $state->id;
            $log->save();

            $state->on_state = 1;
            $state-> save();

            $obj->on_state = 1;
            event(new SendHdmiState($obj));
        } else if($event == 'SendHdmiState'){
            $obj->on_state = $request['on_state'];
            event(new SendHdmiState($obj));
        }
    }
}