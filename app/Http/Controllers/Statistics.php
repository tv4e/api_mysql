<?php

namespace App\Http\Controllers;

use Psy\Util\Json;
use Validator;
use App\Box;
use App\InformationSource;
use App\Asgie;
use App\Log;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Events\SendVideos;
use DB;

class Statistics extends Controller
{
    /**
     *
     * Returns all the seen videos for all users in a given time frame
     *
     * @param string $type Can be notified, library, or injected
     * @param string $from Starting date
     * @param string $to Optional end date, if empty current date is used
     * @param int $rating Optional can be -1, 0 or 1
     * @param int $seenFrom Optional minimum seen time, if empty its set to 1
     * @param int $seenTo Optional maximum seen time, if empty its set to 100
     *
     * Used by the dashboard
     *
     */
    public function seenVideosBox(Request $request)
    {
        $validate = [];
        $validate['type'] = 'required|string|max:20|in:notified,library,injected';
        $validate['from'] = 'required|date';
        $validate['to'] = 'date';
        $validate['rating'] = 'numeric|between:-1,1';
        $validate['seenFrom'] = 'numeric|between:1,100';
        $validate['seenTo'] = 'numeric|between:1,100';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $type = $request['type'];

        $from = Carbon::parse($request['from'])->startOfDay();
        $to = Carbon::parse($request['to'])->endOfDay();

        $rating = $request['rating'];
        $seenFrom = $request['seenFrom'];
        $seenTo = $request['seenTo'];

        if ($type == 'notified') {
            $type = 'SEEN VIDEO NOTIFICATION';
        } else if ($type == 'library') {
            $type = 'SEEN VIDEO LIBRARY';
        } else if ($type == 'injected') {
            $type = 'SEEN VIDEO INJECTED';
        }

        if (!isset($to)) {
            $to = Carbon::now();
        }


        $boxes = DB::table('boxes')
            ->select('uu_id', 'id')
            ->get();

        $asgie = DB::table('asgie')
            ->select('*')
            ->get();

        $data = [];

        foreach ($boxes as $box) {

            $result = [];

            // Get videos for each box total
            $watchedVideosBoxLibraryAsgieRaw = DB::table('logs')
                ->select(
                    (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                    (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS asgie_title')),
                    (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS asgie_title_pt')),
                    (DB::raw('CONCAT(COALESCE(a2.color,""), "", COALESCE(a1.color,"")) AS asgie_color')),
                    'informative_videos.id',
                    'informative_videos.title',
                    'box_informative_video.rating',
                    'box_informative_video.seen',
                    'logs.updated_at as date',
                    'box_informative_video.send_type'
                )
                ->leftJoin('informative_videos',
                    'logs.informative_video_id', '=', 'informative_videos.id')
//                ->leftJoin('box_informative_video', 'logs.informative_video_id',
//                    '=', 'box_informative_video.informative_video_id')
                ->join('box_informative_video', function ($join) use ($box) {
                    $join->on('box_informative_video.informative_video_id', '=', 'logs.informative_video_id')
                        ->where('box_informative_video.box_id', '=', $box->id);
                })
                ->leftJoin('information_sources',
                    'informative_videos.information_source_id', '=', 'information_sources.id')
                ->leftJoin('information_sources_subs',
                    'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                ->where('logs.box_id', $box->id)
                //important because if carta social can be null, cant be forced, and needs to be same as type
                ->where(function ($query) use ($request) {
                    $query->where('box_informative_video.send_type', '!=', 'forced')
                        ->orWhere('box_informative_video.send_type', '=', NULL);
                })
                ->where('logs.event', $type)
                ->whereBetween('logs.updated_at', [$from, $to]);


            if (!empty($rating) || $rating == '0') {
                $watchedVideosBoxLibraryAsgieRaw->where('box_informative_video.rating', $rating);
            }

            if (!empty($seenFrom) && !empty($seenTo)) {
                $watchedVideosBoxLibraryAsgieRaw->whereBetween('box_informative_video.seen', [$seenFrom, $seenTo]);
            }

            $watchedVideosBoxLibraryAsgieRaw = $watchedVideosBoxLibraryAsgieRaw->get()->toArray();

            // sort by asgie
            usort($watchedVideosBoxLibraryAsgieRaw, function ($a, $b) {
                return $a->asgie_id <=> $b->asgie_id;
            });

            foreach ($watchedVideosBoxLibraryAsgieRaw as $key => $value) {
                if ($key == 0) {
                    $result[] = [
                        'asgie_id' => $value->asgie_id,
                        'asgie_title' => $value->asgie_title,
                        'asgie_title_pt' => $value->asgie_title_pt,
                        'asgie_color' => $value->asgie_color,
                        'data' => [[
                            'video_id' => $value->id,
                            'video_title' => $value->title,
                            'date' => $value->date,
                        ]]
                    ];
                } else {
                    if ($value->asgie_id != $result[count($result) - 1]['asgie_id']) {
                        $result[] = [
                            'asgie_id' => $value->asgie_id,
                            'asgie_title' => $value->asgie_title,
                            'asgie_title_pt' => $value->asgie_title_pt,
                            'asgie_color' => $value->asgie_color,
                            'data' => [[
                                'video_id' => $value->id,
                                'video_title' => $value->title,
                                'date' => $value->date,
                            ]]
                        ];
                    }

                    $flagExists = false;
                    $insertKey2 = null;
                    $insertKey3 = null;

                    foreach ($result as $key2 => $value2) {
                        foreach ($value2['data'] as $key3 => $value3) {
                            if ($value->title == $value3['video_title']) {
                                $flagExists = true;
                                $insertKey2 = $key2;
                                $insertKey3 = $key3;
                            } else {
                                $insertKey2 = $key2;
                            }
                        }
                    }

                    if (!$flagExists) {
                        $result[$insertKey2]['data'][] = [
                            'video_id' => $value->id,
                            'video_title' => $value->title,
                            'date' => $value->date,
                        ];
                    }
                }

            }

            $data[] = [
                'uu_id' => $box->uu_id,
                'box_id' => $box->id,
                'data' => []
            ];

            foreach ($asgie as $k1 => $v1) {
                $data[count($data) - 1]['data'][] = [
                    'asgie_id' => $v1->id,
                    'asgie_title' => $v1->title,
                    'asgie_title_pt' => $v1->title_pt,
                    'asgie_color' => $v1->color,
                    'data' => []
                ];

                foreach ($result as $v2) {
                    if (intval($v2['asgie_id']) === intval($v1->id)) {
                        $data[count($data) - 1]['data'][$k1] = $v2;
                    }
                }
            }

        }

        return response(array('success' => true, 'data' => $data));
    }

    /**
     *
     * Returns all the unseen videos for all users in a given time frame
     *
     * @param string $type Can be notified or injected
     * @param string $from Starting date
     * @param string $to Optional end date, if empty current date is used
     *
     * Used by the dashboard
     *
     */
    public function unseenVideosBox(Request $request)
    {
        $validate = [];
        $validate['from'] = 'required|date';
        $validate['to'] = 'date';
        $validate['type'] = 'required|string|max:20|in:notified,injected';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $from = Carbon::parse($request['from'])->startOfDay();
        $to = Carbon::parse($request['to'])->endOfDay();
        $type = $request['type'];

        if (!isset($to)) {
            $to = Carbon::now();
        }

        $boxes = DB::table('boxes')
            ->select('uu_id', 'id')
            ->get();

        $asgie = DB::table('asgie')
            ->select('*')
            ->get();

        $data = [];

        foreach ($boxes as $box) {

            $result = [];

            // Get videos for each box total
            $watchedVideosBoxLibraryAsgieRaw = DB::table('logs')
                ->leftJoin('informative_videos', 'logs.informative_video_id',
                    '=',
                    'informative_videos.id')
//
                ->join('box_informative_video', function ($join) use ($box) {
                    $join->on('box_informative_video.informative_video_id', '=', 'logs.informative_video_id')
                        ->where('box_informative_video.box_id', '=', $box->id);
                })
                ->leftJoin('information_sources',
                    'informative_videos.information_source_id', '=', 'information_sources.id')
                ->leftJoin('information_sources_subs',
                    'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                ->where(function ($query) use ($request) {
                    $query->where('box_informative_video.send_type', '!=', 'forced')
                        ->orWhere('box_informative_video.send_type', '=', NULL);
                })
                ->where('logs.box_id', $box->id)
                ->where('box_informative_video.box_id', $box->id)
                ->where('box_informative_video.send_type', '=', $type)
                ->where('logs.event', 'REJECTED VIDEO')
                ->whereBetween(
                    'logs.updated_at',
                    [
                        $from,
                        $to
                    ]
                )
                ->select(
                    (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                    (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS asgie_title')),
                    (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS asgie_title_pt')),
                    (DB::raw('CONCAT(COALESCE(a2.color,""), "", COALESCE(a1.color,"")) AS asgie_color')),
                    'informative_videos.id',
                    'informative_videos.title',
                    'logs.updated_at as date'
                )
                ->get()
                ->toArray();

            usort($watchedVideosBoxLibraryAsgieRaw, function ($a, $b) {
                return $a->asgie_id <=> $b->asgie_id;
            });

            foreach ($watchedVideosBoxLibraryAsgieRaw as $key => $value) {
                if ($key == 0) {
                    $result[] = [
                        'asgie_id' => $value->asgie_id,
                        'asgie_title' => $value->asgie_title,
                        'asgie_title_pt' => $value->asgie_title_pt,
                        'asgie_color' => $value->asgie_color,
                        'data' => [[
                            'video_id' => $value->id,
                            'video_title' => $value->title,
                            'date' => $value->date,
                        ]]
                    ];
                } else {
                    if ($value->asgie_id != $result[count($result) - 1]['asgie_id']) {
                        $result[] = [
                            'asgie_id' => $value->asgie_id,
                            'asgie_title' => $value->asgie_title,
                            'asgie_title_pt' => $value->asgie_title_pt,
                            'asgie_color' => $value->asgie_color,
                            'data' => [[
                                'video_id' => $value->id,
                                'video_title' => $value->title,
                                'date' => $value->date,
                            ]]
                        ];
                    }

                    $flagExists = false;
                    $insertKey2 = null;
                    $insertKey3 = null;

                    foreach ($result as $key2 => $value2) {
                        foreach ($value2['data'] as $key3 => $value3) {
                            if ($value->title == $value3['video_title']) {
                                $flagExists = true;
                                $insertKey2 = $key2;
                                $insertKey3 = $key3;
                            } else {
                                $insertKey2 = $key2;
                            }
                        }
                    }

                    if (!$flagExists) {
                        $result[$insertKey2]['data'][] = [
                            'video_id' => $value->id,
                            'video_title' => $value->title,
                            'date' => $value->date
                        ];
                    }
                }

            }

            $data[] = [
                'uu_id' => $box->uu_id,
                'box_id' => $box->id,
                'data' => []
            ];

            foreach ($asgie as $k1 => $v1) {
                $data[count($data) - 1]['data'][] = [
                    'asgie_id' => $v1->id,
                    'asgie_title' => $v1->title,
                    'asgie_title_pt' => $v1->title_pt,
                    'asgie_color' => $v1->color,
                    'data' => []
                ];

                foreach ($result as $v2) {
                    if (intval($v2['asgie_id']) === intval($v1->id)) {
                        $data[count($data) - 1]['data'][$k1] = $v2;
                    }
                }
            }

        }

        return response(array('success' => true, 'data' => $data));
    }

    /**
     *
     * Returns all the videos that were rewatched on the video library for all users in a given time frame
     *
     * @param string $type Can be notified or injected
     * @param string $from Starting date
     * @param string $to Optional end date, if empty current date is used
     * @param int $rating Optional can be -1, 0 or 1
     * Used by the dashboard
     *
     */
    public function rewatchedVideosBoxLibrary(Request $request)
    {

        $validate = [];
        $validate['type'] = 'required|string|max:20|in:notified,injected';
        $validate['from'] = 'required|date';
        $validate['to'] = 'date';
        $validate['rating'] = 'numeric|between:-1,1';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $type = $request['type'];

        $from = Carbon::parse($request['from'])->startOfDay();
        $to = Carbon::parse($request['to'])->endOfDay();

        $rating = $request['rating'];

        if (!isset($to)) {
            $to = Carbon::now();
        }

        $boxes = DB::table('boxes')
            ->select('uu_id', 'id')
            ->get();

        $asgie = DB::table('asgie')
            ->select('*')
            ->get();

        $data = [];

        foreach ($boxes as $box) {
            $result = [];

            // Get videos for each box total
            $rewatchedVideosBoxLibraryAsgieRaw = DB::table('logs')
                ->select(
                    (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                    (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS asgie_title')),
                    (DB::raw('CONCAT(COALESCE(a2.color,""), "", COALESCE(a1.color,"")) AS asgie_color')),
                    (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS asgie_title_pt')),
                    'informative_videos.id',
                    'informative_videos.title',
                    'logs.updated_at as date',
                    'box_informative_video.rating'
                )
                ->leftJoin('boxes', 'logs.box_id', '=', 'boxes.id')
                ->leftJoin('informative_videos', 'logs.informative_video_id',
                    '=',
                    'informative_videos.id')
                ->join('box_informative_video', function ($join) use ($box) {
                    $join->on('box_informative_video.informative_video_id', '=', 'logs.informative_video_id')
                        ->where('box_informative_video.box_id', '=', $box->id);
                })
                ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
                ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                ->where(function ($query) use ($type) {
                    $query->where('box_informative_video.send_type', '=', $type)
                        ->orWhere('box_informative_video.send_type', '=', NULL);
                })
                ->where('logs.box_id', $box->id)
                ->where('box_informative_video.box_id', $box->id)
                ->where('logs.event', 'REWATCHED VIDEO LIBRARY')
                ->whereBetween(
                    'logs.updated_at',
                    [
                        $from,
                        $to
                    ]
                );

            if (!empty($rating) || $rating == '0') {
                $rewatchedVideosBoxLibraryAsgieRaw->where('box_informative_video.rating', $rating);
            }

            $rewatchedVideosBoxLibraryAsgieRaw = $rewatchedVideosBoxLibraryAsgieRaw->get()->toArray();

            usort($rewatchedVideosBoxLibraryAsgieRaw, function ($a, $b) {
                return $a->asgie_id <=> $b->asgie_id;
            });

            foreach ($rewatchedVideosBoxLibraryAsgieRaw as $key => $value) {
                if ($key == 0) {
                    $result[] = [
                        'asgie_id' => $value->asgie_id,
                        'asgie_title' => $value->asgie_title,
                        'asgie_title_pt' => $value->asgie_title_pt,
                        'asgie_color' => $value->asgie_color,
                        'data' => [[
                            'video_id' => $value->id,
                            'video_title' => $value->title,
                            'rewatched' => 1,
                            'rewatchedHistory' => [$value->date]
                        ]]
                    ];
                } else {
                    if ($value->asgie_id != $result[count($result) - 1]['asgie_id']) {
                        $result[] = [
                            'asgie_id' => $value->asgie_id,
                            'asgie_title' => $value->asgie_title,
                            'asgie_title_pt' => $value->asgie_title_pt,
                            'asgie_color' => $value->asgie_color,
                            'data' => [[
                                'video_id' => $value->id,
                                'video_title' => $value->title,
                                'rewatched' => 0,
                                'rewatchedHistory' => []
                            ]]
                        ];
                    }

                    $flagExists = false;
                    $insertKey2 = null;
                    $insertKey3 = null;

                    foreach ($result as $key2 => $value2) {
                        foreach ($value2['data'] as $key3 => $value3) {
                            if ($value->title == $value3['video_title']) {
                                $flagExists = true;
                                $insertKey2 = $key2;
                                $insertKey3 = $key3;
                            } else {
                                $insertKey2 = $key2;
                            }
                        }
                    }

                    if (!$flagExists) {
                        $result[$insertKey2]['data'][] = [
                            'video_id' => $value->id,
                            'video_title' => $value->title,
                            'rewatched' => 1,
                            'rewatchedHistory' => [$value->date]
                        ];
                    } else {
                        $result[$insertKey2]['data'][$insertKey3]['rewatchedHistory'][] = $value->date;
                        $result[$insertKey2]['data'][$insertKey3]['rewatched'] = $result[$insertKey2]['data'][$insertKey3]['rewatched'] + 1;
                    }
                }
            }

            $data[] = [
                'uu_id' => $box->uu_id,
                'box_id' => $box->id,
                'data' => []
            ];

            foreach ($asgie as $k1 => $v1) {
                $data[count($data) - 1]['data'][] = [
                    'asgie_id' => $v1->id,
                    'asgie_title' => $v1->title,
                    'asgie_title_pt' => $v1->title_pt,
                    'asgie_color' => $v1->color,
                    'data' => []
                ];

                foreach ($result as $v2) {
                    if (intval($v2['asgie_id']) === intval($v1->id)) {
                        $data[count($data) - 1]['data'][$k1] = $v2;
                    }
                }
            }

        }

        return response(array('success' => true, 'data' => $data));
    }

    /**
     *
     * Returns all videos separated by the seen and unseen for all users
     *
     * Used by the dashboard
     *
     */
    public function unseenSeenVideosTotal($type)
    {
        // Get videos from sources
        $seenVideosTotal = DB::table('box_informative_video')
            ->select(
                (DB::raw('COUNT("seen") as seen'))
            )
            ->whereNotNull('sent_at')
            ->whereNotNull('seen')
            ->where('send_type', $type)
            ->get();

        $unseenVideosTotal = DB::table('box_informative_video')
            ->select(
                (DB::raw('COUNT("seen") as seen'))
            )
            ->whereNotNull('sent_at')
            ->whereNull('seen')
            ->where('send_type', $type)
            ->get();

        $unseenSeenVideosTotal = new \stdClass();
        $unseenSeenVideosTotal->seenVideos = $seenVideosTotal;
        $unseenSeenVideosTotal->unseenVideos = $unseenVideosTotal;

        return response(array('success' => true, 'data' => $unseenSeenVideosTotal));
    }

    /**
     *
     * Returns the number of characters used each month in the amazon polly API
     *
     * Used by the dashboard
     *
     */
    public function pollyChars()
    {
//        $totalTitle = DB::table('informative_videos')
//            ->selectRaw('SUM(CHARACTER_LENGTH(informative_videos.title)) as title_chars')
//            ->selectRaw("MONTH(created_at) AS month")
//            ->selectRaw("YEAR(created_at) AS year")
//            ->groupBy(DB::raw("MONTH(created_at)"), DB::raw("YEAR(created_at)"))
//            ->get();
//
//        $totalDesc = DB::table('informative_videos')
//            ->where('desc', 'NOT LIKE', '%["%')
//            ->selectRaw('SUM(CHARACTER_LENGTH(informative_videos.title)) as desc_chars')
//            ->selectRaw("MONTH(created_at) AS month")
//            ->selectRaw("YEAR(created_at) AS year")
//            ->groupBy(DB::raw("MONTH(created_at)"), DB::raw("YEAR(created_at)"))
//            ->get();
//
//        $totalChars = (object) array_merge((array) $totalTitle, (array) $totalDesc);

        $totalChars = DB::table('informative_videos')
            ->where('desc', 'NOT LIKE', '%["%')
            ->selectRaw('(
                    SUM(CHARACTER_LENGTH(informative_videos.title)) 
                                        +
                    SUM(CHARACTER_LENGTH(informative_videos.desc))
                    ) as chars')
            ->selectRaw("MONTH(created_at) AS month")
            ->selectRaw("YEAR(created_at) AS year")
            ->groupBy(DB::raw("MONTH(created_at)"), DB::raw("YEAR(created_at)"))
            ->get();

        return ($totalChars);
    }

    /**
     *
     * Returns the
     *
     * Used by the dashboard
     *
     */
    public function videos()
    {
        $videos1 = DB::table('informative_videos')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->select(
                'informative_videos.id AS video_id',
                'informative_videos.title AS video_title',
                'informative_videos.desc AS video_desc',
                'informative_videos.created_at AS video_date_creation',
                (DB::raw('CONCAT(COALESCE(information_sources.city_id,""), "", COALESCE(information_sources_subs.city_id,"")) AS video_location')),
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS video_asgie_id')),
                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS video_asgie_title_pt')),
                (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS video_asgie_title_en'))
            )
            ->where(function ($query) {
                $query->where('information_sources_subs.information_source_id', '!=', InformationSource::where('url', 'like', '%cartasocial%')->first()->id)
                    ->orWhere('information_sources_subs.information_source_id', null);
            })
            ->get();

        $videos2 = DB::table('informative_videos')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->select(
                'informative_videos.id AS video_id',
                'informative_videos.title AS video_title',
                'informative_videos.desc AS video_desc',
                'informative_videos.created_at AS video_date_creation',
                (DB::raw('CONCAT(COALESCE(information_sources.city_id,""), "", COALESCE(information_sources_subs.city_id,"")) AS video_location')),
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS video_asgie_id')),
                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS video_asgie_title_pt')),
                (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS video_asgie_title_en'))
            )
            ->where(function ($query) {
                $query->where('information_sources_subs.information_source_id', '=', InformationSource::where('url', 'like', '%cartasocial%')->first()->id);
            })
            ->get();

        $uniqueVideo2 = $videos2->unique('video_title');

        $videos = $videos1->merge($uniqueVideo2);

        return response()->json($videos);
    }

    /**
     *
     * Returns created videos sorted by ASGIE
     *
     * Used by the dashboard
     *
     */
    public function createdVideosAsgie()
    {

        $asgie = DB::table('asgie')
            ->select('*')
            ->get();

        $data = [];

        // Get videos from sources
        $asgieVideosSource = DB::table('informative_videos')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->select(
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS asgie_title')),
                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS asgie_title_pt')),
                (DB::raw('COUNT("asgie_id") as total'))
            )
            ->groupBy('asgie_id', 'asgie_title', 'asgie_title_pt')
            ->orderBy('asgie_id', 'asc')
            ->get();

        foreach ($asgie as $value1) {
            $dataItems = new \stdClass();
            $dataItems->asgie_title = $value1->title;
            $dataItems->asgie_title_pt = $value1->title_pt;
            $dataItems->asgie_id = $value1->id;
            $dataItems->total = 0;
            foreach ($asgieVideosSource as $value2) {
                if (intval($value2->asgie_id) === intval($value1->id)) {
                    $dataItems->total = $value2->total;
                }
            }
            $data[] = $dataItems;
        }

        return response(array('success' => true, 'data' => $data));
    }

    /**
     *
     * Returns the rejected videos sorted by ASGIE
     *
     * Used by the dashboard
     *
     */
    public function unseenVideosAsgie()
    {
        $asgie = DB::table('asgie')
            ->select('*')
            ->get();

        $data = [];

        // Get videos from sources
        $rejectedAsgieVideosSource = DB::table('informative_videos')
            ->leftJoin('box_informative_video', 'informative_videos.id',
                '=',
                'box_informative_video.informative_video_id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->whereNotNull('sent_at')
            ->whereNull('seen')
            ->where('send_type', 'notified')
            ->groupBy('asgie_id', 'asgie_title', 'asgie_title_pt')
            ->orderBy('asgie_id', 'asc')
            ->select(
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS asgie_title')),
                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS asgie_title_pt')),
                (DB::raw('COUNT("asgie_id") as total'))
            )
            ->get();

        foreach ($asgie as $value1) {
            $dataItems = new \stdClass();
            $dataItems->asgie_title = $value1->title;
            $dataItems->asgie_title_pt = $value1->title_pt;
            $dataItems->asgie_id = $value1->id;
            $dataItems->total = 0;
            foreach ($rejectedAsgieVideosSource as $value2) {
                if (intval($value2->asgie_id) === intval($value1->id)) {
                    $dataItems->total = $value2->total;
                }

            }
            $data[] = $dataItems;
        }

        return response(array('success' => true, 'data' => $data));
    }

    /**
     *
     * Returns the watched videos sorted by ASGIE
     *
     * Used by the dashboard
     *
     */
    public function seenVideosAsgie()
    {
        $asgie = DB::table('asgie')
            ->select('*')
            ->get();

        $data = [];

        // Get videos from sources
        $seenAsgieVideosSource = DB::table('informative_videos')
            ->leftJoin('box_informative_video', 'informative_videos.id',
                '=',
                'box_informative_video.informative_video_id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->select(
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS asgie_title')),
                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS asgie_title_pt')),
                (DB::raw('COUNT("asgie_id") as total'))
            )
            ->whereNotNull('sent_at')
            ->whereNotNull('seen')
            ->where('send_type', 'notified')
            ->groupBy('asgie_id', 'asgie_title', 'asgie_title_pt')
            ->orderBy('asgie_id', 'asc')
            ->get();

        foreach ($asgie as $value1) {
            $dataItems = new \stdClass();
            $dataItems->asgie_title = $value1->title;
            $dataItems->asgie_title_pt = $value1->title_pt;
            $dataItems->asgie_id = $value1->id;
            $dataItems->total = 0;
            foreach ($seenAsgieVideosSource as $value2) {
                if (intval($value2->asgie_id) === intval($value1->id)) {
                    $dataItems->total = $value2->total;
                }

            }
            $data[] = $dataItems;
        }

        return response(array('success' => true, 'data' => $data));
    }
}
