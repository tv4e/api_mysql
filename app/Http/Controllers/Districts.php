<?php

namespace App\Http\Controllers;

use Validator;

use App\District;
use Illuminate\Http\Request;

class Districts extends Controller
{
    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Return all portuguese districts
     *
     * Districts come with each associated city
     *
     */
    public function allDistricts()
    {
        $districts = District::with('cities')
            ->get();

        return response($districts)
            ->header('Content-Type', 'application/json');
    }
}
