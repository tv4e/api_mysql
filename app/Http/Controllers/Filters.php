<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Filter;

class Filters extends Controller
{
    /**
     *
     * Return all the keywords used to filter which videos are created
     *
     * Used by the video builder, if the description contains a keyword the video is created
     *
     */
    public function allFilters()
    {
        $filters = Filter::all();

        $response = response($filters)
            ->header('Content-Type', 'application/json');

        return $response;
    }
}