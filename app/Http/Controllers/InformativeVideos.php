<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Validator;

use App\InformativeVideo;
use App\InformationSourcesSub;
use App\AvResource;
use App\AvResourcesType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InformativeVideos extends Controller
{
    public $restful = true;

    public function __construct(){}

    /**
     *
     * Create a new informative video entry and add it to the CDN
     *
     * Used by the video builder after it process the video
     *
     */
    public function postVideo(Request $request)
    {

        if(!isset($request['source_id']) && !isset($request['source_sub_id'])){
            return response(array('success' => false, 'data' => 'No id set'));
        }

        $validate = [];
        $validate['desc'] = 'required|string|max:10000';
        $validate['filename'] = 'required|string';
        $validate['title'] = 'required|string|max:500';
        $validate['duration'] = 'required|int';
        $validate['source_id'] = 'integer';
        $validate['source_sub_id'] = 'integer';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $resource = AvResource::create([
            'url' => env('CDN'). env('CDN_VIDEO') . 'informative/' . $request['filename'],
            'av_resources_type_id' => AvResourcesType::where('type','video')->first()->id
        ]);

        $video = InformativeVideo::create([
            'desc' => $request['desc'],
            'title' => $request['title'],
            'duration' => $request['duration'],
            'information_source_id' => $request['source_id'],
            'information_sources_sub_id' => $request['source_sub_id'],
            'av_resource_id' => $resource->id
        ]);


        $boxes = DB::table('boxes')
            ->select('id')
            ->get();

        foreach ($boxes as $box){
            $cityVerification = true;

            if(isset($request['source_id'])){
                $sourceCity = DB::table('information_sources')
                    ->select('*')
                    ->where('id', $request['source_id'])
                    ->first()
                    ->city_id;

                $sourceId = $request['source_id'];

            } else if (isset($request['source_sub_id'])){
                $source = DB::table('information_sources_subs')
                    ->select('*')
                    ->where('id', $request['source_sub_id'])
                    ->first();

                $sourceCity = $source->city_id;
                $sourceId = $source->information_source_id;

                if(!isset($sourceCity)){
                    $sourceCity = DB::table('information_sources')
                        ->select('*')
                        ->where('id', $sourceId)
                        ->first()
                        ->city_id;
                }
            }

            $asgie_id =  DB::table('information_sources')
                ->select('asgie_id')
                ->where('id', $sourceId)
                ->first()
                ->asgie_id;


            $city = DB::table('boxes')
                ->select('city_id')
                ->where('id', $box->id)
                ->first()
                ->city_id;


            if(isset($sourceCity)){
                if($sourceCity != $city){
                    $cityVerification = false;
                }
            }

//            $score = DB::table('scores')
//                ->select('*')
//                ->where('box_id','=', $box->id)
//                ->where('asgie_id','=', $asgie_id)8
//                ->first();

            if($cityVerification) {

                DB::table('box_informative_video')
                    ->insert([
                        'box_id' => $box->id,
                        'informative_video_id' => $video->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
            }
        }

        return response(array('success' => true, 'video' => env('CDN'). env('CDN_VIDEO') . 'informative/' . $request['filename']));

    }

    /**
     *
     * Check if a video with the given title from a source or sub source already exists in the database
     *
     * Used by the video builder to filter videos it already created
     *
     */
    public function checkVideo(Request $request)
    {
        $validate = [];
        $validate['title'] = 'required|string|max:500';
        $validate['source'] = 'required|integer';
        $validate['sub'] = 'bool';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $title = $request['title'];
        $source = $request['source'];

        if($request['sub']){
            $compare = [
                ['title', '=', $title],
                ['information_sources_sub_id', '=', $source]
            ];

            $cartaSocial = DB::table('information_sources_subs')
                ->leftJoin('information_sources', 'information_sources_subs.information_source_id', '=', 'information_sources.id')
                ->where('information_sources_subs.id', $request['source'])
                ->select('information_sources.url')
                ->first()->url;

            if (strpos($cartaSocial, 'cartasocial') !== false) {
                $sources = InformativeVideo::where($compare)
                    // Check if carta social video was created more than 15 days ago
                    ->whereBetween(
                        'created_at',
                        [
                            Carbon::now()->subDays(15),
                            Carbon::now()
                        ]
                    )
                    ->first();
            }else{
                $sources = InformativeVideo::where($compare)
                    ->first();
            }

        } else {

            $compare = [
                ['title', '=', $title],
                ['information_source_id', '=', $source],
            ];


            $sources = InformativeVideo::where($compare)
                ->first();

        }

        if (empty($sources)) {
            $response =
                [
                    'status' => 'NOK'
                ];
        } else {
            $response =
                [
                    'status' => 'OK'
                ];
        }

        return response($response)
            ->header('Content-Type', 'application/json');
    }
}
