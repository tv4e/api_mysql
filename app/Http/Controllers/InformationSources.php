<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use phpseclib\File\ASN1;
use Validator;

use App\Asgie;
use App\City;
use App\InformationSource;
use App\InformationSourcesSub;
use App\InformativeVideo;

use Illuminate\Http\Request;

class InformationSources extends Controller
{
    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Returns all the av resources associated with the asgie
     *
     * @param  int  $id  The id of the asgie
     *
     */
    public function allSources()
    {
        $sources = InformationSource::with('relAsgie')
            ->with('city')
            ->with(['relSubs' => function ($query) {
                $query->select( '*', 'information_sources_subs.id as subId', 'asgie.title as asgie_title');
                $query->leftJoin('carta_social', 'information_sources_subs.carta_social_id','=','carta_social.id');
                $query->leftJoin('cities', 'information_sources_subs.city_id','=','cities.id');
                $query->leftJoin('asgie', 'information_sources_subs.asgie_id','=','asgie.id');
            }])
            ->get();

        return response($sources)
            ->header('Content-Type', 'application/json');
    }

    /**
     *
     * Returns all the respostas sociais, used to generate carta social videos
     *
     */
    public function getRespostaSocial()
    {
        $respostas = DB::table('carta_social')
            ->get();

        return $respostas;
    }

    /**
     *
     * Add a new information source
     *
     *
     */
    public function postSource(Request $request)
    {
        $validate['url'] = 'required|url';
        $validate['newsContainer'] = 'required|string';
        $validate['newsLink'] = 'required|string';
        $validate['contentContainer'] = 'required|string';
        $validate['title'] = 'required|string';
        $validate['description'] = 'required|string';
        $validate['htmlException'] = 'string';
        $validate['asgie'] = 'required|string';
        $validate['city'] = 'string';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $source = new InformationSource;
        $source->url = $request['url'];
        $source->news_container = $request['newsContainer'];
        $source->news_link = $request['newsLink'];
        $source->content_container = $request['contentContainer'];
        $source->content_title = $request['title'];
        $source->content_description = $request['description'];
        $source->html_exception = $request['htmlException'];
        $source->asgie_id = Asgie::where('title', $request['asgie'])->first()->id;
        $source->city_id = City::where('name', $request['city'])->first()->id;
        $source->save();

        return response($source);
    }

    /**
     *
     * Edit a information source given its id
     *
     * Only necessary to submit the field that are being changed
     *
     */
    public function editSource(Request $request, $id)
    {
        $source = InformationSource::find($id);

        $data = [];
        isset($request['url'])&&$data['url'] = $request['url'];
        isset($request['newsContainer'])&&$data['news_container'] = $request['newsContainer'];
        isset($request['newsLink'])&&$data['news_link'] = $request['newsLink'];
        isset($request['contentContainer'])&&$data['content_container'] = $request['contentContainer'];
        isset($request['title'])&&$data['content_title'] = $request['title'];
        isset($request['description'])&&$data['content_description'] = $request['description'];
        isset($request['htmlException'])&&$data['html_exception'] = $request['htmlException'];
        isset($request['asgie'])&&$data['asgie_id'] = Asgie::where('title', $request['asgie'])->first()->id;
        isset($request['city'])&&$data['city_id'] = City::where('name', $request['city'])->first()->id;

        $source->fill($data);
        $source-> save();

        return response($source);
    }

    /**
     *
     * Delete a information source given its id
     *
     * Warning permanent delete!
     *
     */
    public function deleteSource($id)
    {
        $source = InformationSource::where('id', $id)->delete();
        $video = InformativeVideo::where('information_source_id', $id)->delete();

        return response('success');
    }

    /**
     *
     * Add a new information sub source to an existing information source
     *
     *
     */
    public function postSubSource(Request $request)
    {
        $validate['url'] = 'required';
        $validate['city'] = 'string';
        $validate['information_source_id'] = 'required|int';
        $validate['asgie'] = 'string';
        $validate['respostaSocial'] = 'string';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $subSource = new InformationSourcesSub;
        $subSource->url = $request['url'];
        $subSource->information_source_id = $request['information_source_id'];

        if(isset($request['respostaSocial'])){
            $subSource->carta_social_id = DB::table('carta_social')->where('resposta', $request['respostaSocial'])->first()->id;
        } else {
            $subSource->carta_social_id = 0;
        }

        $subSource->asgie_id = Asgie::where('title', $request['asgie'])->first()->id;
        $subSource->city_id = City::where('name', $request['city'])->first()->id;
        $subSource->save();

        return response($subSource);
    }

    /**
     *
     * Edit a information source given its id
     *
     *
     */
    public function editSubSource(Request $request, $id)
    {
        $subSource = InformationSourcesSub::find($id);

        $data = [];
        isset($request['url'])&&$data['url'] = $request['url'];
        isset($request['respostaSocial'])&&$data['carta_social_id'] = DB::table('carta_social')->where('resposta', $request['respostaSocial'])->first()->id;
        isset($request['information_source_id'])&&$data['information_source_id'] = $request['information_source_id'];
        isset($request['asgie'])&&$data['asgie_id'] = Asgie::where('title', $request['asgie'])->first()->id;
        isset($request['city'])&&$data['city_id'] = City::where('name', $request['city'])->first()->id;

        $subSource->fill($data);
        $subSource-> save();

        return response($subSource);
    }

    /**
     *
     * Delete a information sub source given its id
     *
     * Warning permanent delete!
     *
     */
    public function deleteSubSource($id)
    {
        $source = InformationSourcesSub::where('id', $id)->delete();
        $video = InformativeVideo::where('information_sources_sub_id', $id)->delete();

        return response('success');
    }

    /**
     *
     * Returns HTLM content of an URL used to perform webcrawling of the dashboard
     *
     *
     */
    public function getURL(Request $request)
    {
        $validate['url'] = 'required|url';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $contents = file_get_contents($request['url']);

        return $contents;
    }
}
