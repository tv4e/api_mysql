<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\InformativeVideo;
use App\InformationSource;

class Recommendations extends Controller
{
    /**
     *
     * Returns all videos, with the exception of carta social
     *
     * Used by the recommendations API Majordomo
     *
     */
    public function videos()
    {
        $videos = DB::table('informative_videos')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->select(
                'informative_videos.id AS video_id',
                'informative_videos.title AS video_title',
                'informative_videos.desc AS video_desc',
                'informative_videos.duration AS duration',
                'informative_videos.created_at AS video_date_creation',
                (DB::raw('CONCAT(COALESCE(information_sources.city_id,""), "", COALESCE(information_sources_subs.city_id,"")) AS video_location')),
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS video_asgie_id')),
                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS video_asgie_title_pt')),
                (DB::raw('CONCAT(COALESCE(a2.title,""), "", COALESCE(a1.title,"")) AS video_asgie_title_en'))
            )
            ->where(function ($query) {
                $query->where('information_sources_subs.information_source_id', '!=', InformationSource::where('url', 'like', '%cartasocial%')->first()->id)
                    ->orWhere('information_sources_subs.information_source_id', null);
            })
            ->get();

        return response()->json($videos);
    }

    /**
     *
     * Returns the ratings and seen % of all videos each user watched
     *
     * Used by the recommendations API Majordomo
     *
     */
    public function ratings()
    {
        $ratings = DB::table('box_informative_video')
            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                '=',
                'informative_videos.id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->where([
                ['box_informative_video.seen', '>', 0],
                ['box_informative_video.sent_at', '!=', null],
                ['box_informative_video.send_type', '!=', null]
            ])
            ->where(function ($query) {
                $query->where('information_sources_subs.information_source_id', '!=', InformationSource::where('url', 'like', '%cartasocial%')->first()->id)
                    ->orWhere('information_sources_subs.information_source_id', null);
            })
            ->select(
                'box_informative_video.box_id AS user_id',
                'box_informative_video.informative_video_id AS video_id',
                'box_informative_video.seen AS video_watch_time',
                'box_informative_video.sent_at AS rating_date_creation',
                'box_informative_video.rating AS rating_value',
                'box_informative_video.send_type AS video_watched_type')
            ->get();

        foreach ($ratings as $key => $value) {
            $log = DB::table('logs')
                ->select('*')
                ->where([
                    ['box_id', '=', $value->user_id],
                    ['informative_video_id', '=', $value->video_id],
                    ['event', '=', 'SEEN VIDEO LIBRARY']
                ])
                ->get();

            if (isset($log[0])) {
                $value->video_watched_type = 'library';
            }
        }

        return response()->json($ratings);
    }

    /**
     *
     * Returns all the users, their data and
     *
     * Used by the recommendations API Majordomo
     *
     */
    public function users()
    {
        $users = DB::table('boxes')
            ->select(
                'boxes.id AS user_id',
                'boxes.age As user_age',
                'boxes.user As user_name',
                'boxes.gender AS user_gender',
                'boxes.city_id',
                'boxes.coordinates AS user_coordinates')
            ->get();

        foreach ($users as $key => $value) {
            $logs = DB::table('logs')
                ->select('*')
                ->where('box_id', $value->user_id)
                ->where(function ($query) {
                    $query->where('event', 'not like', '%VIDEO%')
                        ->orWhere('event', '=', null);
                })
                ->where(function ($query) {
                    $query->where('event', 'not like', '%KEY%')
                        ->orWhere('event', '=', null);
                })
                ->get();

            $value->logs = $logs;
        }


        return response()->json($users);
    }

//    public function videos2()
//    {
//        $videos = DB::connection('mysql2')
//            ->select('SELECT * FROM `majordomo_videos`');
//
//        return response()->json($videos);
//    }
//
//    public function ratings2()
//    {
//        $ratings = DB::connection('mysql2')
//            ->select('SELECT * FROM `majordomo_ratings`');
//
//        foreach ($ratings as $key => $value) {
//            $log = DB::connection('mysql2')
//                ->select('SELECT * FROM `logs` WHERE `box_id` = ' . $value->box_id .
//                    ' AND `informative_video_id`= ' . $value->video_id .
//                    ' AND `event` LIKE "SEEN VIDEO LIBRARY"');
//
//            if (isset($log[0])) {
//                $value->video_watched_type = 'library';
//            }
//
//        }
//
//        return response()->json($ratings);
//    }
//
//    public function all()
//    {
//        $ratings = DB::connection('mysql2')
//            ->select('SELECT * FROM `majordomo_videos_all`');
//
//        foreach ($ratings as $key => $value) {
//            $log = DB::connection('mysql2')
//                ->select('SELECT * FROM `logs` WHERE `box_id` = ' . $value->box_id .
//                    ' AND `informative_video_id`= ' . $value->video_id .
//                    ' AND `event` LIKE "SEEN VIDEO LIBRARY"');
//
//            if (isset($log[0])) {
//                $value->video_watched_type = 'library';
//                $value->rating_date_creation = $log[0]->created_at;
//            }
//        }
//
//        foreach ($ratings as $key => $value) {
//            $asgie = null;
//
//            $informative_videos = DB::connection('mysql2')
//                ->select('SELECT * FROM `informative_videos` WHERE `id` = ' . $value->video_id);
//
//            foreach ($informative_videos as $key2 => $value2) {
//
//                if ($value2->information_sources_sub_id == NULL) {
//                    $asgie = DB::connection('mysql2')
//                        ->select('SELECT * FROM `information_sources` WHERE `id` = ' . $value2->information_source_id);
//
//                    $asgie = $asgie[0]->asgie_id;
//                }
//
//                if ($value2->information_source_id == NULL) {
//                    $asgie = DB::connection('mysql2')
//                        ->select('SELECT * FROM `information_sources_subs` WHERE `id` = ' . $value2->information_sources_sub_id);
//
//                    $asgie = $asgie[0]->asgie_id;
//                }
//            }
//
//            $value->asgie_id = $asgie;
//
//        }
//
//        return response()->json($ratings);
//    }
//
//    public function users2()
//    {
//        $users = DB::connection('mysql2')
//            ->select('SELECT * FROM `majordomo_users`');
//
//
//        return response()->json($users);
//    }
}
