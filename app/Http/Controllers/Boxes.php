<?php

namespace App\Http\Controllers;

use App\Events\AppReady;
use App\Events\RefreshContent;
use App\Events\SendHdmiState;
use App\Events\RefreshLib;
use App\InformationSource;
use App\City;
use App\Channel;
use Psy\Util\Json;
use Validator;
use App\Box;
use App\Log;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Events\SendVideos;
use App\Events\SendKey;
use App\Events\RefreshChannels;
use DB;
use GooglePlaces;
use Illuminate\Console\Command;


class Boxes extends Controller
{

    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Returns all the user boxes with city name
     *
     */
    public function boxes()
    {
        $box = Box::all();

        foreach ($box as $key => $value) {
            $value["city"] =  City::where('id', $value->city_id)->first()->name;
        }

        return response($box, 200);
    }

    /**
     *
     * Returns a box given its id
     *
     */
    public function box($serial)
    {
        $box = Box::where('serial', $serial)
            ->first();

        return response($box, 200);
    }

    /**
     *
     * Returns the videos of a box given its serial number
     *
     * Normal videos from the last 5 days and social videos from the last 15 days
     *
     */
    public function videos($serial)
    {
        $ruleOfFifths = 5;
        $box = Box::where('serial', $serial)->first();

        $sentVideos =DB::table('box_informative_video')
            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                '=','informative_videos.id')
            ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->where([
                ['box_id', '=', $box->id]
            ])
            ->where(function ($query) {
                $query->where('information_sources_subs.information_source_id', '!=', InformationSource::where('url','like','%cartasocial%')->first()->id)
                    ->orWhere('information_sources_subs.information_source_id',  null);
            })
            ->whereBetween(
                'box_informative_video.sent_at',
                [
                    Carbon::now()->subDays($ruleOfFifths),
                    Carbon::now()
                ]
            )
            ->whereNull('informative_videos.expired_at')
            ->whereNotNull('sent_at')
            ->select(
                'box_informative_video.sent_at as date',
                'av_resources.url',
                'informative_videos.id',
                'informative_videos.expired_at',
                (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                'informative_videos.duration',
                'informative_videos.title',
                'box_informative_video.seen as type'
            )
            ->orderBy('box_informative_video.updated_at', 'desc')
            ->get();

        //THE VALUE 15 is also in the informative videos check
        //This value is related to the video creation periodicy, so new carta social in 15 to 15 days
        $socialVideos = DB::table('box_informative_video')
            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                '=', 'informative_videos.id')
            ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->where([
                ['box_id', '=', $box->id],
                ['information_sources_subs.information_source_id', '=', InformationSource::where('url','like','%cartasocial%')->first()->id],
                [DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), '=', $box->city_id],
            ])
            ->whereBetween(
                'informative_videos.created_at',
                [
                    Carbon::now()->subDays(15),
                    Carbon::now()
                ]
            )
            ->whereNull('informative_videos.expired_at')
            ->select(
                'av_resources.url',
                'informative_videos.id',
                'informative_videos.expired_at',
                (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                'informative_videos.duration',
                'informative_videos.created_at as date',
                'informative_videos.title',
                'box_informative_video.seen as type')
            ->orderBy('informative_videos.created_at', 'desc')
            ->get();

        foreach ($sentVideos as $value) {
            if($value->type === null){
                $value->type="unseen";
            } else {
                $value->type="seen";
            }
        }

        foreach ($socialVideos as $value) {
            if($value->type === null){
                $value->type="unseen";
            } else {
                $value->type="seen";
            }
        }

        $response = [
            'sentVideos' => $sentVideos,
            'socialVideos' => $socialVideos
        ];

        return response($response, 200);
    }

    /**
     *
     * Update the on_state of a box
     *
     * Also creates a log entry and send a socket event
     *
     */
    public function state(Request $request, $serial)
    {
        $validate['on_state'] = 'required|boolean';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        // LOG FILE ON STATE
        $foo = Box::where('serial', $serial)->first();
        $log = new Log;
        $log->on_state = $request['on_state'];
        $log->box_id = $foo->id;
        $log->save();

        $box = Box::where('serial', $serial)->first();
        $box->fill($request->all());
        $box-> save();

        $obj = new \stdClass();
        $obj->serial=$serial;
        $obj->on_state=$request['on_state'];
        event(new SendHdmiState($obj));

        return response($box, 200);
    }

    /**
     *
     * Update DB after a video is watched by a user
     *
     * Also creates a log entry and socket event
     *
     */
    public function videoState(Request $request, $serial)
    {
        $data = $request->all();

        $validate['seen'] = 'required|int';
        $validate['informative_video_id'] = 'required|int';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $box = Box::where('serial', $serial)->first();

        // Check if the video was already seen and only update in case the new value is higher
        $seenBefore = DB::table('box_informative_video')
            ->select('*')
            ->where([
                ['box_id', '=', $box->id],
                ['informative_video_id', '=', $data['informative_video_id']]
            ])->first()->seen;

        if(intval($data['seen']) > intval($seenBefore) || $seenBefore==null){
            DB::table('box_informative_video')
                ->where([
                    ['box_id', '=', $box->id],
                    ['informative_video_id', '=', $data['informative_video_id']]
                ])
                ->update([
                    'seen' => $data['seen'],
                    'updated_at' => Carbon::now()
                ]);
        }

        $obj = new \stdClass();
        $obj->serial = $serial;
        $obj->videos = $this->videos($serial);

        // Socket event to refresh the video library after the video is watched
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        $connection =  @socket_connect($socket, '127.0.0.1', 6379);

        if( $connection ){
            event(new RefreshLib($obj));
            $msg = 'success';
            $status = 200;
        } else {
            $msg = 'socket error';
            $status = 500;
        }

        return response($msg, $status);
    }

    /**
     *
     * Returns the nearest open pharmacy within 10km of the box's coordinates
     *
     * Uses google places api
     *
     */
    public function farmacias(Request $request, $serial)
    {
        $box=Box::where('serial', $serial)->first();
        $coordinates = $box->coordinates;
        $places = GooglePlaces::nearbySearch($coordinates, 10000, array("type"=>'pharmacy', "language"=>'pt', "opennow"));

        $result = [];

        for ($i = 0; $i < sizeof($places); $i++){
            $placeID = $places['results'][$i]['place_id'];
            $result = GooglePlaces::placeDetails($placeID, array("language"=>'pt'));

            if(isset($result['result']['formatted_phone_number'])) {
                break;
            }
        }

        return response($result, 200);
    }

    /**
     *
     * Returns the nearest taxi service within 10km of the box's coordinates
     *
     * Uses google places api
     *
     */
    public function taxis(Request $request, $serial)
    {
        $box = Box::where('serial', $serial)->first();
        $coordinates = $box->coordinates;
        $places = GooglePlaces::nearbySearch($coordinates, 10000, array("keyword"=>'taxi', "language"=>'pt'));

        $result = [];

        for ($i = 0; $i < sizeof($places); $i++){
            $placeID = $places['results'][$i]['place_id'];
            $result = GooglePlaces::placeDetails($placeID, array("language"=>'pt'));

            if(isset($result['result']['formatted_phone_number'])) {
                break;
            }
        }

        return response($result, 200);
    }

    /**
     *
     * Returns the last channel the box watched
     *
     */
    public function lastChannel(Request $request, $serial)
    {
        $data = $request->all();

        $validate['channel'] = 'required|int';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        DB::table('boxes')
            ->where([
                ['serial', '=', $serial],
            ])
            ->update([
                'last_channel' => $data['channel'],
            ]);

        return response($data['channel'], 200);
    }

    /**
     *
     * Manually send the most recent video to a box given its serial number
     *
     */
    public function sendVideo(Request $request, $serial){

        $msg = "";
        $sendSocialToday = true;

        $box = Box::where('serial',$serial)->first();

        $err = "";
        $videoToSend = [];

        $sent_videosToday = DB::table('box_informative_video')
            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                '=',
                'informative_videos.id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->select(
                'information_sources_subs.url as subUrl',
                'information_sources_subs.information_source_id as subId',
                'seen'
            )
            ->where('box_id', '=', $box->id)
            ->whereBetween('sent_at', [Carbon::today(), Carbon::tomorrow()])
            ->get();


        //Check if sent one cartaSocial video today
        foreach($sent_videosToday as $value){
            if ($value->subId != null){
                if (strpos(InformationSource::where('id',$value->subId)->first()->url, 'cartasocial') !== false) {
                    $sendSocialToday = false;
                }
            }
        }

        if($sendSocialToday){
            //Check if every carta social video has been sent through a week
            //Send Carta Social Video

            $videoToSend = DB::table('box_informative_video')
                ->leftJoin('boxes','box_informative_video.box_id','=','boxes.id')
                ->leftJoin('informative_videos','box_informative_video.informative_video_id',
                    '=',
                    'informative_videos.id')
                ->leftJoin('av_resources','informative_videos.av_resource_id','=','av_resources.id')
                ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
                ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
                ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                ->select(
                    (DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id) AS city')),
                    (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                    (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                    'informative_videos.title',
                    'boxes.serial',
                    'av_resources.url',
                    'box_informative_video.informative_video_id')
                ->where([
                    ['box_id', '=', $box->id],
                    ['information_sources_subs.information_source_id', '=', InformationSource::where('url','like','%cartasocial%')->first()->id],
                    ['seen',  '=', null],
                    [DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), '=', $box->city_id],
                    ['sent_at', '=', null]
                ])
                ->whereBetween(
                    'informative_videos.created_at',
                    [
                        Carbon::now()->subDays(15),
                        Carbon::now()
                    ]
                )
                ->first();

            $msg = 'Video sent of Carta Social';
        }

        if ($videoToSend == null || !$sendSocialToday){
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'http://79.137.39.168:8080/majordomo/fast_user_recommendations/'.$box->id);
            $stream = $response->getBody();
            $stream->rewind(); // Seek to the beginning
            $contents = $stream->getContents(); // returns all the contents
            $contents = json_decode($contents, true);

            // Check if request failed
            if (isset($contents["data"])) {

                $videoID = null;

                // Check the data for an unseen video
                foreach ($contents["data"] as $video) {
                    $checkVideo = DB::table('box_informative_video')
                        ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                            '=',
                            'informative_videos.id')
                        ->where([
                            ['box_id', '=', $video["user_id"]],
                            ['informative_video_id', '=', $video["video_id"]],
                            ['seen', '=', NULL],
                            ['sent_at', '=', NULL],
                            ['informative_videos.expired_at', '=', NULL]
                        ])
                        ->exists();

                    if ($checkVideo == true) {
                        $videoID = $video["video_id"];
                        break;
                    }
                }

                // Check if there is a valid video
                if ($videoID != null) {

                    // Get rest of video data to send
                    $videoToSend = DB::table('box_informative_video')
                        ->select('information_sources_subs.city_id as subCity',
                            'information_sources.city_id as city',
                            (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                            (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                            'informative_videos.title',
                            'boxes.serial',
                            'av_resources.url',
                            'box_informative_video.informative_video_id')
                        ->leftJoin('boxes', 'box_informative_video.box_id', '=', 'boxes.id')
                        ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                            '=',
                            'informative_videos.id')
                        ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
                        ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
                        ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                        ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                        ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                        ->where([
                            ['box_id', '=', $box->id],
                            ['informative_video_id', '=', $videoID],
                        ])
                        ->whereNull('sent_at')
                        ->first();

                    $msg = 'Video sent via Majordomo ';
                } else {
                    $err = 'FAIL';
                }
            } else {
                $err = 'FAIL';
            }

            // Fallback video sending
            if( $err == 'FAIL'){
                $videoToSend = DB::table('box_informative_video')
                    ->select(
                        (DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id) AS city')),
                        (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                        (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                        'informative_videos.title',
                        'boxes.serial',
                        'av_resources.url',
                        'box_informative_video.informative_video_id')
                    ->leftJoin('boxes','box_informative_video.box_id','=','boxes.id')
                    ->leftJoin('informative_videos','box_informative_video.informative_video_id',
                        '=',
                        'informative_videos.id')
                    ->leftJoin('av_resources','informative_videos.av_resource_id','=','av_resources.id')
                    ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
                    ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
                    ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                    ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                    ->where([
                        ['box_id', '=', $box->id],
                        ['seen',  '=', null],
                        ['informative_videos.expired_at',  '=', null],
                        ['box_informative_video.sent_at',  '=', null],
                        ['information_sources_subs.information_source_id', '!=', InformationSource::where('url','like','%cartasocial%')->first()->id]
                    ])
                    ->where(function ($query) use($box) {
                        $query->where(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), $box->city_id)
                            ->orwhere(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), null);
                    })
                    ->orderBy('informative_videos.created_at', 'desc')
                    ->first();

                $msg = 'Video sent with fallback';
            }
        }

        //FIX THE FALLBACK BECASUE VIDEOS NOT ASSOCIATED ARE BEING SELECTED
        event(new SendVideos($videoToSend));

        $boxValues = DB::table('box_informative_video')
        ->where([
            ['box_id', '=', $box->id],
            ['informative_video_id', '=', $videoToSend->informative_video_id]
        ])
        ->update([
            'sent_at' => Carbon::now(),
            'send_type' => 'forced'
        ]);

        return response()->json($videoToSend, 200);
    }

    /**
     *
     * Reset all the watched videos of a box given its serial number
     *
     * Resets box_informative_videos and logs tables
     *
     */
    public function reset(Request $request, $serial)
    {

        $box = Box::where('serial', $serial)
            ->first();

        $resetVideos = DB::table('box_informative_video')
            ->where([
                ['box_id', '=', $box->id]
            ])
            ->update([
                'sent_at' => NULL,
                'seen' => NULL,
                'send_type' => NULL,
                'rating' => NULL
            ]);

        $resetLogs = DB::table('logs')
            ->where([
                ['box_id', '=', $box->id]
            ])
            ->delete();

        return response('delete sucess', 200);
    }

    /**
     *
     * Used during testing to delete logs with errors
     *
     * Will stay here in case of emergency
     *
     */
    public function damageControl(Request $request, $serial)
    {

        $validate = [];
        $validate['from'] = 'required|date';
        $validate['to'] = 'required|date';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $box = Box::where('serial', $serial)
            ->first();


        $logs =  DB::table('logs')
            ->where([
                ['box_id', '=', $box->id]
            ])
            ->whereBetween(
                'logs.updated_at',
                [
                    $request['from'],
                    $request['to']
                ]
            )
            ->orderBy('updated_at', 'asc')
            ->select('*')
            ->get();

        $countEvent=0;
        $erroredVideos=[];

        foreach ($logs as $k=>$v){
            if($v->event=='SEEN VIDEO INJECTED' && $logs[$k-1]->event == 'RECEIVED VIDEO'){
                $countEvent++;
                if($countEvent >= 4){
                    $erroredVideos[] = $v->informative_video_id;
                }
            } else if($v->event !='SEEN VIDEO INJECTED' && $v->event != 'RECEIVED VIDEO'){
                $countEvent=0;
            }
        }

        foreach ($erroredVideos as $v){
            $resetVideos = DB::table('box_informative_video')
                ->where([
                    ['box_id', '=', $box->id],
                    ['informative_video_id', '=', $v]
                ])
                ->update([
                    'sent_at' => NULL,
                    'seen' => NULL,
                    'send_type' => NULL,
                    'rating' => NULL
                ]);

            $resetLogs = DB::table('logs')
                ->where([
                    ['box_id', '=', $box->id],
                    ['informative_video_id', '=', $v]
                ])
                ->delete();
        }

        return response($erroredVideos, 200);
    }

    /**
     *
     * Add a new box to the database
     *
     */
    public function postBox(Request $request)
    {
        $validate['uu_id'] = 'required|int|unique:boxes';
        $validate['serial'] = 'required|string|unique:boxes';
        $validate['city'] = 'required|string';
        $validate['user'] = 'required|string';
        $validate['age'] = 'required|int';
        $validate['gender'] = 'required|string';
        $validate['coordinates'] = 'required|string';
        $validate['iptv_type'] = 'required|string';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $box = new Box;
        $box->uu_id = $request['uu_id'];
        $box->serial = $request['serial'];
        $box->on_state = 0;
        $box->city_id = City::where('name', $request['city'])->first()->id;
        $box->user = $request['user'];
        $box->age = $request['age'];
        $box->gender = $request['gender'];
        $box->coordinates = $request['coordinates'];
        $box->last_channel = 1;
        $box->iptv_type = $request['coordinates'];
        $box->save();

        return response($box);
    }

    /**
     *
     * Edit an existing box
     *
     */
    public function putBox(Request $request, $id)
    {
        $validate['uu_id'] = 'int|unique:boxes';
        $validate['serial'] = 'string|unique:boxes';
        $validate['on_state'] = 'boolean';
        $validate['city'] = 'string';
        $validate['user'] = 'string';
        $validate['age'] = 'int';
        $validate['gender'] = 'string';
        $validate['coordinates'] = 'string';
        $validate['iptv_type'] = 'string';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $data = [];

        isset($request['uu_id'])&&$data['uu_id'] = $request['uu_id'];
        isset($request['serial'])&&$data['serial'] = $request['serial'];
        isset($request['on_state'])&&$data['on_state'] = $request['on_state'];
        isset($request['city'])&&$data['city_id'] = City::where('name', $request['city'])->first()->id;
        isset($request['user'])&&$data['user'] = $request['user'];
        isset($request['age'])&&$data['age'] = $request['age'];
        isset($request['gender'])&&$data['gender'] = $request['gender'];
        isset($request['coordinates'])&&$data['coordinates'] = $request['coordinates'];
        isset($request['last_channel'])&&$data['last_channel'] = $request['last_channel'];
        isset($request['iptv_type'])&&$data['iptv_type'] = $request['iptv_type'];

        $box = Box::find($id);
        $box->fill($data);
        $box-> save();

        if( isset($request['on_state'])){
            $obj = new \stdClass();
            $obj->serial = Box::where('id', $id)->first()->serial;
            $obj->on_state = $request['on_state'];
            event(new SendHdmiState($obj));
        }

        $obj = new \stdClass();
        $obj->id = 1;
        $obj->data = $this->boxes();
        event(new RefreshContent($obj));

        return response($box);
    }

    /**
     *
     * Update the video rating when the rating screen appears
     *
     */
    public function videoRating(Request $request, $serial)
    {
        $data = $request->all();

        $validate['rating'] = 'required|int';
        $validate['informative_video_id'] = 'required|int';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $box = Box::where('serial', $serial)->first();

        $rating = DB::table('box_informative_video')
            ->where([
                ['box_id', '=', $box->id],
                ['informative_video_id', '=', $data['informative_video_id']]
            ])
            ->whereNotNull('seen')
            ->update(['rating' => $data['rating']]);

        return response($rating, 200);
    }

    /**
     *
     * Update the on_state of a box without sending a socket event afterward
     *
     */
    public function stateNoEvent(Request $request, $serial)
    {
        $validate['on_state'] = 'required|bool';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }


        Box::where('serial', $serial)
            ->update(['on_state' => $request['on_state']]);


        return $request['on_state'];
    }

    /**
     *
     * Used by the cron to determine which video to sent to a box
     *
     * First it verifies if the box already received a social video, if not it asks the recommendation
     * algorithm for a video and in case that fails it simply sends the most recent video
     *
     */
    public function getVideo($serial)
    {
        $msg = "empty mind";
        $sendSocialToday = true;
        $box = Box::where('serial', $serial)
            ->first();

        $err = false;
        $videoToSend = [];

        $sent_videosToday = DB::table('box_informative_video')
            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                '=',
                'informative_videos.id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->select(
                'information_sources_subs.url as subUrl',
                'information_sources_subs.information_source_id as subId',
                'seen'
            )
            ->where('box_id', '=', $box->id)
            ->whereBetween('sent_at', [Carbon::today(), Carbon::tomorrow()])
            ->get();

        //Check if sent one cartaSocial video today
        foreach($sent_videosToday as $value){
            if ($value->subId != null){
                if (strpos(InformationSource::where('id',$value->subId)->first()->url, 'cartasocial') !== false) {
                    $sendSocialToday = false;
                }
            }
        }

        if($sendSocialToday){
            //Check if every carta social video has been sent through a week
            //Get Carta Social Video
            $videoToSend = DB::table('box_informative_video')
                ->leftJoin('boxes','box_informative_video.box_id','=','boxes.id')
                ->leftJoin('informative_videos','box_informative_video.informative_video_id',
                    '=',
                    'informative_videos.id')
                ->leftJoin('av_resources','informative_videos.av_resource_id','=','av_resources.id')
                ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
                ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
                ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                ->select(
                    (DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id) AS city')),
                    (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                    (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                    'informative_videos.title',
                    'boxes.serial',
                    'av_resources.url',
                    'box_informative_video.informative_video_id')
                ->where([
                    ['box_id', '=', $box->id],
                    ['information_sources_subs.information_source_id', '=', InformationSource::where('url','like','%cartasocial%')->first()->id],
                    ['seen',  '=', null],
                    [DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), '=', $box->city_id],
                    ['sent_at', '=', null]
                ])
                ->whereBetween(
                    'informative_videos.created_at',
                    [
                        Carbon::now()->subDays(15),
                        Carbon::now()
                    ]
                )
                ->first();

            if($videoToSend != null){
                $msg = 'Video '.$videoToSend->informative_video_id.' sent of Carta Social to box '.$box->id.' | ';
            }
        }

        if ($videoToSend == null || !$sendSocialToday){
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'http://79.137.39.168:8080/majordomo/fast_user_recommendations/'.$box->id);
            $stream = $response->getBody();
            $stream->rewind(); // Seek to the beginning
            $contents = $stream->getContents(); // returns all the contents
            $contents = json_decode($contents, true);

            // Check if request failed
            if (isset($contents["data"])) {

                $videoID = null;

                // Check the data for an unseen video
                foreach ($contents["data"] as $video) {
                    $checkVideo = DB::table('box_informative_video')
                        ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                            '=',
                            'informative_videos.id')
                        ->where([
                            ['box_id', '=', $video["user_id"]],
                            ['informative_video_id', '=', $video["video_id"]],
                            ['seen', '=', NULL],
                            ['sent_at', '=', NULL],
                            ['informative_videos.expired_at', '=', NULL]
                        ])
                        ->exists();

                    if ($checkVideo == true) {
                        $videoID = $video["video_id"];
                        break;
                    }
                }

                // Check if there is a valid video
                if ($videoID != null) {

                    // Get rest of video data to send
                    $videoToSend = DB::table('box_informative_video')
                        ->select('information_sources_subs.city_id as subCity',
                            'information_sources.city_id as city',
                            (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                            (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                            'informative_videos.title',
                            'boxes.serial',
                            'av_resources.url',
                            'box_informative_video.informative_video_id')
                        ->leftJoin('boxes', 'box_informative_video.box_id', '=', 'boxes.id')
                        ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                            '=',
                            'informative_videos.id')
                        ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
                        ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
                        ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                        ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                        ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                        ->where([
                            ['box_id', '=', $box->id],
                            ['on_state', '=', 1],
                            ['informative_video_id', '=', $videoID],
                        ])
                        ->whereNull('sent_at')
                        ->first();

                    if($videoToSend != null){
                        $msg = 'Video '.$videoToSend->informative_video_id.' sent via Majordomo to box '.$box->id.' | ';
                    }
                } else {
                    $err = true;
                }
            } else {
                $err = true;
            }

            // Fallback video sending
            if( $err == true || $videoToSend == null){
                $videoToSend = DB::table('box_informative_video')
                    ->select(
                        (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                        (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                        'informative_videos.title',
                        'boxes.serial',
                        'av_resources.url',
                        'box_informative_video.informative_video_id')
                    ->leftJoin('boxes','box_informative_video.box_id','=','boxes.id')
                    ->leftJoin('informative_videos','box_informative_video.informative_video_id',
                        '=',
                        'informative_videos.id')
                    ->leftJoin('av_resources','informative_videos.av_resource_id','=','av_resources.id')
                    ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
                    ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
                    ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                    ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                    ->where([
                        ['box_id', '=', $box->id],
                        ['seen',  '=', null],
                        ['box_informative_video.sent_at',  '=', null],
                        ['informative_videos.expired_at', '=', null],
                    ])
                    ->where(function ($query)  {
                        $query->where('information_sources_subs.information_source_id', '!=', InformationSource::where('url','like','%cartasocial%')->first()->id)
                            ->orwhere('information_sources_subs.information_source_id', '=', null);
                    })
                    ->where(function ($query) use($box) {
                        $query->where(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), $box->city_id)
                            ->orwhere(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), null);
                    })
                    ->orderBy('informative_videos.created_at', 'desc')
                    ->first();

                if($videoToSend == null) {
                    $msg = 'Video '.$videoToSend->informative_video_id.'  sent with fallback to box '.$box->id.' | ';
                }
            }

            if($videoToSend == null) {
                $msg = 'Failed to send video to box '.$box->id.' | ';
            }
        }

        if($videoToSend != null) {
            event(new SendVideos($videoToSend));

            if($box->iptv_type == "iptv"){
                $sendType = 'notified';
            } else {
                $sendType = 'injected';
            }

            DB::table('box_informative_video')
                ->where([
                    ['box_id', '=', $box->id],
                    ['informative_video_id', '=', $videoToSend->informative_video_id]
                ])
                ->update([
                    'sent_at' => Carbon::now(),
                    'send_type' => $sendType
                ]);
        } else {
            $msg = 'Failed to send video to box '.$box->id.' | ';
        }

        return $msg;
    }

    /**
     *
     * Similar to the reset function but it also adds existing video to the box
     *
     * Useful when adding a new box to the database and immediately populate it with videos
     *
     */
    public function resetAndAddVideos($id)
    {

        DB::table('box_informative_video')->where('box_id', $id)->delete();

        $videosToAdd = [];

        $box = Box::where('id', $id)
            ->first();

        $videos = DB::table('informative_videos')
            ->select(
                'informative_videos.id')
            ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
            ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->where([
                ['informative_videos.expired_at', '=', null],
            ])
            ->where(function ($query) use($box) {
                $query->where(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), $box->city_id)
                    ->orwhere(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), null);
            })
            ->orderBy('informative_videos.created_at', 'desc')
            ->get();


        foreach ($videos as $v){
            $videosToAdd[] = [
                'box_id' => $id,
                'informative_video_id'=>$v->id,
                'seen'=> NULL,
                'sent_at'=> NULL,
                'send_type'=> NULL,
                'rating'=> NULL,
            ];
        }


        DB::table('box_informative_video')
            ->insert($videosToAdd);

        return response('reset was a ........................................ sucess', 200);
    }

    /**
     *
     * Sends a keycode to the iTV application via socket which in turn is able
     * to process it as if the user pressed a key on the remote
     *
     */
    public function sendKey(Request $request, $serial)
    {
        $validate['keyCode'] = 'required';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }
    
        $obj = new \stdClass();
        $obj->serial=$serial;
        $obj->key = $request['keyCode'];

        event(new SendKey($obj));

        return response('sucess', 200);
    }

    /**
     *
     * Returns the list of TV channels
     *
     */
    public function getChannels()
    {
        $data = Channel::orderBy('number', 'asc')
            ->get();

        return response($data, 200);
    }

    /**
     *
     * Add a new TV channel
     *
     */
    public function postChannel(Request $request)
    {
        $validate['name'] = 'required|string';
        $validate['number'] = 'required|int|unique:channels';
        $validate['stream'] = 'string';
        $validate['av_resource_id'] = 'required|int';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $channel = new Channel;
        $channel->name = $request['name'];
        $channel->number = $request['number'];
        $channel->stream = $request['stream'];
        $channel->av_resource_id = $request['av_resource_id'];
        $channel->save();

//        $this->refreshChannels();

        return response($channel);
    }

    /**
     *
     * Edit a new TV channel
     *
     */
    public function putChannel(Request $request, $id)
    {
        $validate['name'] = 'string';
        $validate['number'] = 'int|unique:channels';
        $validate['stream'] = 'string';
        $validate['av_resource_id'] = 'int';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $data = [];

        isset($request['name'])&&$data['name'] = $request['name'];
        isset($request['number'])&&$data['number'] = $request['number'];
        isset($request['stream'])&&$data['stream'] = $request['stream'];
        isset($request['av_resource_id'])&&$data['av_resource_id'] = $request['av_resource_id'];

        $channel = Channel::find($id);
        $channel->fill($data);
        $channel-> save();

//        $this->refreshChannels();

        return response($channel);
    }

    /**
     *
     * Sends a socket event to all boxes to update their current channel list with the list in the DB
     *
     */
    public function refreshChannels()
    {
        $boxes = Box::all();

        $channels = Channel::orderBy('number', 'asc')
            ->get();

        foreach ($boxes as $box) {
            $obj = new \stdClass();
            $obj->serial = $box['serial'];
            $obj->channels = $channels;
            event(new RefreshChannels($obj));
        }
    }

    /**
     *
     * Returns the user's video list given the box serial
     *
     * Used by the mobile app
     *
     */
    public function appVideos($serial)
    {
        $box = Box::where('serial', $serial)->first();

        $sentVideos =DB::table('box_informative_video')
            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                '=',
                'informative_videos.id')
            ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
            ->where([
                ['box_id', '=', $box->id]
            ])
            ->whereBetween(
                'box_informative_video.sent_at',
                [
                    Carbon::now()->subDays(5),
                    Carbon::now()
                ]
            )
            ->whereNull('informative_videos.expired_at')
            ->whereNotNull('sent_at')
            ->select('box_informative_video.sent_at as date',
                'av_resources.url',
                'informative_videos.id',
                'informative_videos.expired_at',
                (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                'informative_videos.duration',
                'informative_videos.title'
            )
            ->orderBy('date', 'desc')
            ->get();

        return response($sentVideos, 200);
    }

    /**
     *
     * Returns the recommended video list given the box serial
     *
     * Used by the mobile app
     *
     */
    public function appRecommended($serial)
    {
        $box = Box::where('serial', $serial)->first();

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://79.137.39.168:8080/majordomo/fast_user_recommendations/'.$box->id);
        $stream = $response->getBody();
        $stream->rewind(); // Seek to the beginning
        $contents = $stream->getContents(); // returns all the contents
        $contents = json_decode($contents, true);

        // Check if request failed
        if (isset($contents["data"])) {

            $videos = [];

            foreach ($contents["data"] as $video) {

                $result = DB::table('box_informative_video')
                    ->select(
                        (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                        (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                        (DB::raw('CONCAT(COALESCE(a2.id,""), "", COALESCE(a1.id,"")) AS asgie_id')),
                        'informative_videos.title',
                        'av_resources.url',
                        'informative_videos.duration',
                        'informative_videos.id',
                        'sent_at',
                        'informative_videos.expired_at',
                        'box_informative_video.informative_video_id')
                    ->leftJoin('boxes', 'box_informative_video.box_id', '=', 'boxes.id')
                    ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                        '=',
                        'informative_videos.id')
                    ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
                    ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
                    ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                    ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                    ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                    ->where([
                        ['box_id', '=', $box->id],
                        ['informative_video_id', '=', $video['video_id']],
                    ])
                    ->whereNull('informative_videos.expired_at')
                    ->whereNull('sent_at')
                    ->first();

                if($result){
                    array_push($videos, $result);
                }
            }

            $status = 200;
        } else {
            $status = 500;
        }

        return response($videos, $status);
    }

    /**
     *
     * Used to send a video from the mobile app to the TV
     *
     * Used by the mobile app
     *
     */
    public function sendVideoId(Request $request, $serial){

        $validate['informative_video_id'] = 'int|required';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $box = Box::where('serial', $serial)->first();

        if($box->on_state == 1){
            $videoToSend = DB::table('box_informative_video')
                ->select('information_sources_subs.city_id as subCity',
                    'information_sources.city_id as city',
                    (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
                    (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
                    'informative_videos.title',
                    'boxes.serial',
                    'av_resources.url',
                    'box_informative_video.informative_video_id')
                ->leftJoin('boxes', 'box_informative_video.box_id', '=', 'boxes.id')
                ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
                    '=',
                    'informative_videos.id')
                ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
                ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
                ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
                ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
                ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
                ->where([
                    ['box_id', '=', $box->id],
                    ['informative_video_id', '=', $request['informative_video_id']],
                ])
                ->first();

            if($videoToSend != null)
            {
                event(new SendVideos($videoToSend));

                $boxValues = DB::table('box_informative_video')
                    ->where([
                        ['box_id', '=', $box->id],
                        ['informative_video_id', '=', $request['informative_video_id']]
                    ])
                    ->update([
                        'sent_at' => Carbon::now(),
                        'send_type' => 'mobile'
                    ]);

                $msg = 'Video '.$videoToSend->informative_video_id.' sent via mobile';
            } else {
                $msg = 'Error sending via mobile';
            }
        } else {
            $msg = 'Box is turned off';
        }

        return response($msg, 200);
    }

    /**
     *
     * Updates the DB after a video is watched via the mobile app
     *
     * Used by the mobile app
     *
     */
    public function seenMobile(Request $request, $serial){

        $validate['informative_video_id'] = 'int|required';
        $validate['seen'] = 'int|required';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $box = Box::where('serial', $serial)->first();

        $seenBefore = DB::table('box_informative_video')
            ->select('*')
            ->where([
                ['box_id', '=', $box->id],
                ['informative_video_id', '=', $request['informative_video_id']]
            ])
            ->first()->seen;

        if(intval($request['seen']) > intval($seenBefore) || $seenBefore==null){
            DB::table('box_informative_video')
                ->where([
                    ['box_id', '=', $box->id],
                    ['informative_video_id', '=', $request['informative_video_id']]
                ])
                ->update([
                    'seen' => $request['seen'],
                    'send_type' => 'mobile',
                    'sent_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
        }

        return response('success', 200);
    }

}