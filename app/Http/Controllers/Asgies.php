<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\DB;
use App\Asgie;


class Asgies extends Controller
{
    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Returns all asgie
     *
     */
    public function allAsgie()
    {
        $asgies = Asgie::all();

        $response = response($asgies)
            ->header('Content-Type', 'application/json');

        return $response;
    }

    /**
     *
     * Returns all the av resources associated with the asgie
     *
     * @param  int  $id  The id of the asgie
     *
     */
    public function getAsgie($id)
    {
        //@TODO ELOQUENT thingy here
        $asgies = DB::table('asgie')
            ->select('asgie.title', 'av_resources.url', 'av_resources.name', 'av_resources_types.type', 'asgie_av_resource.selected', 'asgie_av_resource.duration_percentage')
            ->where('asgie.id',$id)
            ->where('av_resources.deleted_at',NULL)
            ->leftJoin('asgie_av_resource','asgie.id','=','asgie_av_resource.asgie_id')
            ->leftJoin('av_resources','asgie_av_resource.av_resource_id','=','av_resources.id')
            ->leftJoin('av_resources_types','av_resources.av_resources_type_id','=','av_resources_types.id')
            ->get();

        if (empty($asgies->all())){
            return [];
        }

        $response = [
            'title' => $asgies->first()->title,
            'resources' => [],
        ];

        foreach ($asgies as $asgie){
            if($asgie->type == 'image'){
                $response['resources'][$asgie->type][]=[
                    'url'=>$asgie->url,
                    'name'=>$asgie->name,
                    'duration'=>$asgie->duration_percentage,
                    'selected'=> $asgie->selected
                ];
            }else{
                $response['resources'][$asgie->type]=[
                    'url'=>$asgie->url,
                    'name'=>$asgie->name,
                ];
            }

        }

        return response()->json($response);
    }
}
