<?php

namespace App\Http\Controllers;

use App\Events\SendVideos;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;
use GuzzleHttp\Client;
use File;
use App\Events\AppReady;
use App\Events\SendHdmiState;
use Psy\Util\Json;
use App\Box;
use App\Log;
use App\InformationSource;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use App\City;
use App\AvResource;
use App\AvResourcesType;

class Testing extends Controller
{
    public function superTesting($id)
    {
        $videos = DB::table('logs')
            ->select(
                'logs.box_id',
                'logs.event',
                'logs.seen',
                'logs.informative_video_id',
                'logs.created_at'
            )
            ->where([
                ['logs.box_id', '=', $id],
                ['logs.event', 'LIKE', '%video%'],
            ])
            ->whereNotNull('logs.seen')
            ->whereBetween(
                'logs.created_at',
                [
                    Carbon::createFromDate(2018, 4, 18),
                    Carbon::createFromDate(2018, 4, 28)
                ]
            )
            ->orderBy('logs.created_at', 'desc')
            ->get();

        return $videos;
    }

    public function megaTesting()
    {

    }
}