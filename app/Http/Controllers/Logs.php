<?php

namespace App\Http\Controllers;

use Validator;
use App\Log;
use App\Box;
use Illuminate\Http\Request;
use DB;

class Logs extends Controller
{
    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Create a new log entry in the database
     *
     * Logs can store an on state, an event such as "VIDEO RECEIVED", the % seen of a video and the video id
     *
     */
    public function state(Request $request, $serial)
    {
        $validate['on_state'] = 'boolean';
        $validate['event'] = 'string|max:250';
        $validate['seen'] = 'int';
        $validate['informative_video_id'] = 'int';

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        $foo = Box::where('serial', $serial)->first();

        $log = new Log;

        if(isset($request['on_state'])){
            $log->on_state = $request['on_state'];
        }

        if(isset($request['event'])){
            $log->event = $request['event'];
        }

        if(isset($request['informative_video_id'])){
            $log->informative_video_id = $request['informative_video_id'];
        }

        if(isset($request['seen'])){
            $log->seen = $request['seen'];
        }

        $log->box_id = $foo->id;

        $log->save();

        return response($log);
    }
}
