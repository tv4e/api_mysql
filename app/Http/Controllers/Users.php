<?php

namespace App\Http\Controllers;


use Validator;
use Illuminate\Http\Request;

class Users extends Controller
{
    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Logs out a logged user
     *
     */
    public function logout(Request $request)
    {
        $user = $request->user();
        $user->token()->revoke();

        $json = [
            'success' => true,
            'code' => 200,
            'message' => 'You are Logged out.',
        ];

        return response()->json($json, '200');
    }

    public function checkToken(Request $request)
    {
        $user = $request->user();

        $json = [
            'success' => true,
            'status' => 200,
            'data' => $user->token()
        ];

        return response()->json($json, '200');
    }

}
