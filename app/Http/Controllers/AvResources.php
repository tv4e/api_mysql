<?php

namespace App\Http\Controllers;

use App\AvResourcesType;
use Illuminate\Support\Facades\Storage;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

use App\Asgie;
use App\AvResource;

use Illuminate\Http\Request;

class AvResources extends Controller
{
    public $restful =  true;

    public function __construct()
    {
    }

    /**
     *
     * Add a new av resource, either video, audio or image
     *
     * The resource can be also added to an asgie
     *
     */
    public function postResource(Request $request)
    {
        $resourcesId = [];
        $validate = [];
        $resource = null;

        if($request['asgie_id']){
            $validate['asgie_id'] = 'integer';
            $validate['selected'] = 'required|boolean';
            $validate['duration'] = 'integer|min:10|max:100';
        } else {
            $validate['audio'] = 'mimetypes:audio/mpeg';
            $validate['video'] = 'mimetypes:video/avi,video/mpeg,video/mp4';
            $validate['image'] = 'mimes:jpeg,bmp,png';
            $validate['name'] = 'string|max:250';
        }

        $validator = Validator::make($request->all(), $validate);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $errors;
        }

        if ($request['image']) {
            $name = str_random(32) . '.jpg';

            $image = Image::make($request['image'])
                ->resize(null, 1080, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->resize(1920, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->encode('jpg', 80);


            Storage::cloud()->put(env('CDN_IMAGE').$name,$image, 'public');

            $resource = AvResource::create([
                'url' => env('CDN'). env('CDN_IMAGE') . $name,
                'name' => $request['name'],
                'av_resources_type_id' => AvResourcesType::where('type','image')->first()->id
            ]);

        } else if($request['video']) {

            $name = str_random(32) . '.' .$request['video']->extension();

            Storage::cloud()->put(env('CDN_VIDEO').$name,  fopen($request['video'], 'r+'), 'public');

            $resource = AvResource::create([
                'url' => env('CDN'). env('CDN_VIDEO') . $name,
                'name' => $request['name'],
                'av_resources_type_id' => AvResourcesType::where('type','video')->first()->id,
                'information_source_id' => $request['source_id']
            ]);
        } else if($request['video-intro']) {
            $name = str_random(32) . '.' .$request['video-intro']->extension();

            Storage::cloud()->put(env('CDN_VIDEO').$name,  fopen($request['video-intro'], 'r+'), 'public');

            $resource = AvResource::create([
                'url' => env('CDN'). env('CDN_VIDEO') . $name,
                'name' => $request['name'],
                'av_resources_type_id' => AvResourcesType::where('type','video-intro')->first()->id,
                'information_source_id' => $request['source_id']
            ]);
        } else if($request['video-outro']) {
            $name = str_random(32) . '.' .$request['video-outro']->extension();

            Storage::cloud()->put(env('CDN_VIDEO').$name,  fopen($request['video-outro'], 'r+'), 'public');

            $resource = AvResource::create([
                'url' => env('CDN').  env('CDN_VIDEO') . $name,
                'name' => $request['name'],
                'av_resources_type_id' => AvResourcesType::where('type','video-outro')->first()->id,
                'information_source_id' => $request['source_id']
            ]);
        } else if($request['audio']) {
            $name = str_random(32) . '.' .$request['audio']->extension();

            Storage::cloud()->put(env('CDN_AUDIO').$name,  fopen($request['audio'], 'r+'), 'public');

            $resource = AvResource::create([
                'url' => env('CDN'). env('CDN_AUDIO') . $name,
                'name' => $request['name'],
                'name' => $request['name'],
                'av_resources_type_id' => AvResourcesType::where('type','audio')->first()->id,
                'information_source_id' => $request['source_id']
            ]);
        }

        if($request['asgie_id']){
            Asgie::find($request['asgie_id'])
                ->resources()
                ->attach($resource->id, array(
                    'selected' => $request['selected'],
                    'duration_percentage' => $request['duration'],
                ));
        }

        return response('Success');
    }

    /**
     *
     * Edit an av resource given its id
     *
     */
    public function putResource(Request $request, $id)
    {
        $resource = AvResource::find($id);
        $resource -> url = $request['url'];
        $resource -> roles() -> attach();
        $save =$resource-> save();

        if($save){
            return response($resource);
        }
    }

    /**
     *
     * Delete an av resource given its id
     *
     * Warning permanent delete!
     *
     */
    public function deleteResource(Request $request)
    {
        $resource = AvResource::where('url', $request['url'])->delete();

        return response($resource);
    }
}
