<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformativeVideo extends Model
{

    /**
     * The roles that belong to the Video.
     */
    public function resources()
    {
        return $this->belongsToMany('App\AvResource', 'informative_video_av_resource', 'informative_video_id','av_resource_id');
    }

    public function boxes()
    {
        return $this->belongsToMany('App\Box');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'desc', 'title', 'information_source_id', 'information_sources_sub_id', 'av_resource_id', 'duration'
    ];

}
