<?php

namespace App\Listeners;

use App\Events\RefreshChannels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshChannelsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RefreshChannels  $event
     * @return void
     */
    public function handle(RefreshChannels $event)
    {
        //
    }
}
