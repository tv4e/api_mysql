<?php

namespace App\Listeners;

use App\Events\SendHdmiState;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HdmiListener
{
    /**
     * Create the event listener.
     *
     \*/
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendHdmiState  $event
     * @return void
     */
    public function handle(SendHdmiState $event)
    {
        //
    }
}
