<?php

namespace App\Listeners;

use App\Events\RefreshLib;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshLibListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RefreshLib  $event
     * @return void
     */
    public function handle(RefreshLib $event)
    {
        //
    }
}
