<?php

namespace App\Listeners;

use App\Events\RefreshContent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshContentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RefreshContent  $event
     * @return void
     */
    public function handle(RefreshContent $event)
    {
        //
    }
}
