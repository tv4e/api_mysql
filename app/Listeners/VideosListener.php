<?php

namespace App\Listeners;

use App\Events\SendVideos;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VideosListener
{
    /**
     * Create the event listener.
     *
     \*/
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendVideos  $event
     * @return void
     */
    public function handle(SendVideos $event)
    {
        //
    }
}
