<?php

namespace App\Listeners;

use App\Events\SendKey;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class KeyListener
{
    /**
     * Create the event listener.
     *
     \*/
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendKey  $event
     * @return void
     */
    public function handle(SendKey $event)
    {
        //
    }
}
