<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvResource extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The videos that belong to the Resource.
     */
    public function videos()
    {
        return $this->belongsToMany('App\InformativeVideo', 'informative_video_av_resource', 'av_resource_id','informative_video_id' );
    }

    /**
     * The asgies that belong to the Resource.
     */
    public function asgies()
    {
        return $this->belongsToMany('App\Asgie', 'asgie_av_resource', 'av_resource_id','asgie_id' )
            ->withPivot( "selected", "duration");
    }

    public function relAvType()
    {
        return $this->belongsTo('App\AvResourcesType', 'av_resources_type_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'name', 'av_resources_type_id'
    ];
}
