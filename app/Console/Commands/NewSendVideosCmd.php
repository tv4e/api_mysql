<?php
//
//namespace App\Console\Commands;
//
//use Illuminate\Console\Command;
//use App\Log;
//use App\Events\SendVideos;
//use App\Box;
//use App\InformationSource;
//use Illuminate\Support\Facades\DB;
//use Carbon\Carbon;
//
//class NewSendVideosCmd extends Command
//{
//    protected $signature = 'newsend:videos';
//
//    protected $description = 'check number of sent videos to box on that day - max days is 5
//         do not send videos with bad score Send videos most recent if most recent is in a space
//         of 5 days send';
//
//    public function __construct()
//    {
//        parent::__construct();
//    }
//
//    public function handle()
//    {
//        $msg = "";
//
//        $ruleOfFifths = 100;
//        $msg = "";
//        $sendSocialToday = true;
//
//        $boxValues = Box::all();
//
//        foreach($boxValues as $box) {
//
//            if($box->on_state == 0){
//                continue;
//            }
//
//            $err = false;
//            $videoToSend = [];
//
//            $sent_videosToday = DB::table('box_informative_video')
//                ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
//                    '=',
//                    'informative_videos.id')
//                ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
//                ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
//                ->select(
//                    'information_sources_subs.url as subUrl',
//                    'information_sources_subs.information_source_id as subId',
//                    'seen'
//                )
//                ->where('box_id', '=', $box->id)
//                ->whereBetween('sent_at', [Carbon::today(), Carbon::tomorrow()])
//                ->get();
//
//            if(sizeof($sent_videosToday) >= $ruleOfFifths){
//                continue;
//            }
//
//            //Check if sent one cartaSocial video today
//            foreach($sent_videosToday as $value){
//                if ($value->subId != null){
//                    if (strpos(InformationSource::where('id',$value->subId)->first()->url, 'cartasocial') !== false) {
//                        $sendSocialToday = false;
//                    }
//                }
//            }
//
//            if($sendSocialToday){
//                //Check if every carta social video has been sent through a week
//                //Get Carta Social Video
//                $videoToSend = DB::table('box_informative_video')
//                    ->leftJoin('boxes','box_informative_video.box_id','=','boxes.id')
//                    ->leftJoin('informative_videos','box_informative_video.informative_video_id',
//                        '=',
//                        'informative_videos.id')
//                    ->leftJoin('av_resources','informative_videos.av_resource_id','=','av_resources.id')
//                    ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
//                    ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
//                    ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
//                    ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
//                    ->select(
//                        (DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id) AS city')),
//                        (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
//                        (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
//                        'informative_videos.title',
//                        'boxes.serial',
//                        'av_resources.url',
//                        'box_informative_video.informative_video_id')
//                    ->where([
//                        ['box_id', '=', $box->id],
//                        ['information_sources_subs.information_source_id', '=', InformationSource::where('url','like','%cartasocial%')->first()->id],
//                        ['seen',  '=', null],
//                        [DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), '=', $box->city_id],
//                        ['sent_at', '=', null]
//                    ])
//                    ->whereBetween(
//                        'informative_videos.created_at',
//                        [
//                            Carbon::now()->subDays(15),
//                            Carbon::now()
//                        ]
//                    )
//                    ->first();
//
//                if($videoToSend != null){
//                    $msg .= 'Video '.$videoToSend->informative_video_id.' sent of Carta Social to box '.$box->id.' | ';
//                }
//            }
//
//            if ($videoToSend == null || !$sendSocialToday){
//                $client = new \GuzzleHttp\Client();
//                $response = $client->request('GET', 'http://79.137.39.168:8080/majordomo/fast_user_recommendations/'.$box->id);
//                $stream = $response->getBody();
//                $stream->rewind(); // Seek to the beginning
//                $contents = $stream->getContents(); // returns all the contents
//                $contents = json_decode($contents, true);
//
//                // Check if request failed
//                if (isset($contents["data"])) {
//
//                    $videoID = null;
//
//                    // Check the data for an unseen video
//                    foreach ($contents["data"] as $video) {
//                        $checkVideo = DB::table('box_informative_video')
//                            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
//                                '=',
//                                'informative_videos.id')
//                            ->where([
//                                ['box_id', '=', $video["user_id"]],
//                                ['informative_video_id', '=', $video["video_id"]],
//                                ['seen', '=', NULL],
//                                ['sent_at', '=', NULL],
//                                ['informative_videos.expired_at', '=', NULL]
//                            ])
//                            ->exists();
//
//                        if ($checkVideo == true) {
//                            $videoID = $video["video_id"];
//                            break;
//                        }
//                    }
//
//                    // Check if there is a valid video
//                    if ($videoID != null) {
//
//                        // Get rest of video data to send
//                        $videoToSend = DB::table('box_informative_video')
//                            ->select('information_sources_subs.city_id as subCity',
//                                'information_sources.city_id as city',
//                                (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
//                                (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
//                                'informative_videos.title',
//                                'boxes.serial',
//                                'av_resources.url',
//                                'box_informative_video.informative_video_id')
//                            ->leftJoin('boxes', 'box_informative_video.box_id', '=', 'boxes.id')
//                            ->leftJoin('informative_videos', 'box_informative_video.informative_video_id',
//                                '=',
//                                'informative_videos.id')
//                            ->leftJoin('av_resources', 'informative_videos.av_resource_id', '=', 'av_resources.id')
//                            ->leftJoin('information_sources', 'informative_videos.information_source_id', '=', 'information_sources.id')
//                            ->leftJoin('information_sources_subs', 'informative_videos.information_sources_sub_id', '=', 'information_sources_subs.id')
//                            ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
//                            ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
//                            ->where([
//                                ['box_id', '=', $box->id],
//                                ['on_state', '=', 1],
//                                ['informative_video_id', '=', $videoID],
//                            ])
//                            ->whereNull('sent_at')
//                            ->first();
//
//                        if($videoToSend != null){
//                            $msg .= 'Video '.$videoToSend->informative_video_id.' sent via Majordomo to box '.$box->id.' | ';
//                        }
//                    } else {
//                        $err = true;
//                    }
//                } else {
//                    $err = true;
//                }
//
//                // Fallback video sending
//                if( $err == true || $videoToSend == null){
//                    $videoToSend = DB::table('box_informative_video')
//                        ->select(
//                            (DB::raw('CONCAT(COALESCE(a2.image,""), "", COALESCE(a1.image,"")) AS image')),
//                            (DB::raw('CONCAT(COALESCE(a2.title_pt,""), "", COALESCE(a1.title_pt,"")) AS title_pt')),
//                            'informative_videos.title',
//                            'boxes.serial',
//                            'av_resources.url',
//                            'box_informative_video.informative_video_id')
//                        ->leftJoin('boxes','box_informative_video.box_id','=','boxes.id')
//                        ->leftJoin('informative_videos','box_informative_video.informative_video_id',
//                            '=',
//                            'informative_videos.id')
//                        ->leftJoin('av_resources','informative_videos.av_resource_id','=','av_resources.id')
//                        ->leftJoin('information_sources','informative_videos.information_source_id','=','information_sources.id')
//                        ->leftJoin('information_sources_subs','informative_videos.information_sources_sub_id','=','information_sources_subs.id')
//                        ->leftJoin('asgie as a1', 'information_sources.asgie_id', '=', 'a1.id')
//                        ->leftJoin('asgie as a2', 'information_sources_subs.asgie_id', '=', 'a2.id')
//                        ->where([
//                            ['box_id', '=', $box->id],
//                            ['seen',  '=', null],
//                            ['box_informative_video.sent_at',  '=', null],
//                            ['informative_videos.expired_at', '=', null],
//                        ])
//                        ->where(function ($query)  {
//                            $query->where('information_sources_subs.information_source_id', '!=', InformationSource::where('url','like','%cartasocial%')->first()->id)
//                                ->orwhere('information_sources_subs.information_source_id', '=', null);
//                        })
//                        ->where(function ($query) use($box) {
//                            $query->where(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), $box->city_id)
//                                ->orwhere(DB::raw('COALESCE(information_sources.city_id, information_sources_subs.city_id)'), null);
//                        })
//                        ->orderBy('informative_videos.created_at', 'desc')
//                        ->first();
//
//                    if($videoToSend == null) {
//                        $msg .= 'Video '.$videoToSend->informative_video_id.'  sent with fallback to box '.$box->id.' | ';
//                    }
//                }
//
//                if($videoToSend == null) {
//                    $msg .= 'Failed to send video to box '.$box->id.' | ';
//                    continue;
//                }
//            }
//
//            if($videoToSend != null) {
//                event(new SendVideos($videoToSend));
//
//                if($box->iptv_type == "iptv"){
//                    $sendType = 'notified';
//                } else {
//                    $sendType = 'injected';
//                }
//
//                DB::table('box_informative_video')
//                    ->where([
//                        ['box_id', '=', $box->id],
//                        ['informative_video_id', '=', $videoToSend->informative_video_id]
//                    ])
//                    ->update([
//                        'sent_at' => Carbon::now(),
//                        'send_type' => $sendType
//                    ]);
//            } else {
//                $msg .= 'Failed to send video to box '.$box->id.' | ';
//            }
//        }
//
//        $log = new Log;
//        $log->event = $msg;
//        $log->save();
//
//        return $msg;
//    }
//}
