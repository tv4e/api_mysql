<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\InformationSource;
use App\Events\SendVideos;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DeletedVideosCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if any videos are older than 30 days and delete them';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('informative_videos')
            ->where('created_at', '<', Carbon::now()->subDays(14))
            ->update(['expired_at' => Carbon::now()]);
    }
}
