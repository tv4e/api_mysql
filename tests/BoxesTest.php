<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BoxesTest extends TestCase
{

    use WithoutMiddleware;
    use DatabaseTransactions;

    var $serial = '4e37ad67f27fa74b';

//    public function testBoxes()
//    {
//        $this->json('GET', 'http://api_mysql.tv4e.pt/api/boxes')
//            ->seeJsonStructure([[
//                "id",
//                "uu_id",
//                "serial",
//                "on_state",
//                "city_id",
//                "age",
//                "gender",
//                "coordinates",
//                "last_channel",
//                "iptv_type",
//                "created_at",
//                "updated_at",
//                "deleted_at"
//            ]])
//            ->seeStatusCode(200);
//    }

//    public function testBox()
//    {
//        $this->json('GET', "http://api_mysql.tv4e.pt/api/boxes/box/".$this->serial)
//            ->seeJsonStructure([
//                "id",
//                "uu_id",
//                "serial",
//                "on_state",
//                "city_id",
//                "age",
//                "gender",
//                "coordinates",
//                "last_channel",
//                "iptv_type",
//                "created_at",
//                "updated_at",
//                "deleted_at"
//            ])
//            ->seeStatusCode(200);
//    }
//
//    public function testVideos()
//    {
//        $this->json('GET', 'http://api_mysql.tv4e.pt/api/boxes/videos/'.$this->serial)
//            ->seeJsonStructure([
//                "videosSeen",
//                "videosUnseen"
//            ])
//            ->seeStatusCode(200);
//    }

//    public function testState()
//    {
//        for ($i = 0; $i <= 1; $i++) {
//            $this->json('PUT', 'http://api_mysql.tv4e.pt/api/boxes/' . $this->serial,
//                ['on_state' => $i])
//                ->seeJson([
//                    "on_state" => $i
//                ])
//                ->seeStatusCode(200);
//        }
//    }

    public function testVideoState()
    {
        $this->json('POST', 'http://api_mysql.tv4e.pt/api/boxes/video/'.$this->serial,
            ['seen' => 99,
                'informative_video_id' => 4355])
            ->see('success')
            ->seeStatusCode(200);
    }

//    public function testLastChannel()
//    {
//        for ($i = 1; $i <= 7; $i++) {
//            $this->json('PUT', 'http://api_mysql.tv4e.pt/api/boxes/channel/'.$this->serial,
//                ['channel' => $i])
//                ->see($i)
//                ->seeStatusCode(200);
//        }
//    }


}
