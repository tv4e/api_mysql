<?php
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'user'], function(){
    Route::get('logout', ['uses' => 'Users@logout'])
        ->middleware('auth:api');
});

Route::group(['prefix'=>'asgies'], function(){
    Route::get('', ['uses' => 'Asgies@allAsgie']);
//        ->middleware('auth:api');
});

Route::group(['prefix'=>'asgie'], function(){
    Route::get('{id}',['uses' => 'Asgies@getAsgie']);
//        ->middleware('auth:api');
});

Route::group(['prefix'=>'sources'], function(){
    Route::get('', ['uses' => 'InformationSources@allSources']);
    Route::get('social', ['uses' => 'InformationSources@getRespostaSocial']);
    Route::post('', ['uses' => 'InformationSources@postSource']);
    Route::put('{id}', ['uses' => 'InformationSources@editSource']);
    Route::delete('{id}', ['uses' => 'InformationSources@deleteSource']);
    Route::post('sub', ['uses' => 'InformationSources@postSubSource']);
    Route::put('sub/{id}', ['uses' => 'InformationSources@editSubSource']);
    Route::delete('sub/{id}', ['uses' => 'InformationSources@deleteSubSource']);
    Route::get('url', ['uses' => 'InformationSources@getURL']);
});

Route::group(['prefix'=>'resources'], function(){
    Route::post('', ['uses' => 'AvResources@postResource'])
        ->middleware('auth:api');
    Route::put('{id}', ['uses' => 'AvResources@putResource'])
        ->middleware('auth:api');
});

Route::group(['prefix'=>'resource'], function(){
    Route::delete('', ['uses' => 'AvResources@deleteResource'])
        ->middleware('auth:api');
});

Route::group(['prefix'=>'districts'], function(){
    Route::get('', ['uses' => 'Districts@allDistricts']);
});

Route::group(['prefix'=>'videos'], function(){
    Route::post('', ['uses' => 'InformativeVideos@postVideo'])
        ->middleware('auth:api');
    Route::get('check', ['uses' => 'InformativeVideos@checkVideo']);
});

Route::group(['prefix'=>'filters'], function(){
    Route::get('', ['uses' => 'Filters@allFilters'])
        ->middleware('auth:api');
});

Route::group(['prefix'=>'boxes'], function(){
    Route::get('', ['uses' => 'Boxes@boxes']);
    Route::get('box/{serial}', ['uses' => 'Boxes@box']);
    Route::get('box/{id}/resetAndAddVideos', ['uses' => 'Boxes@resetAndAddVideos']);
    Route::get('videos/{serial}', ['uses' => 'Boxes@videos']);
    Route::put('{serial}', ['uses' => 'Boxes@state']);
    Route::post('video/{serial}', ['uses' => 'Boxes@videoState']);
    Route::get('farmacias/{serial}', ['uses' => 'Boxes@farmacias']);
    Route::get('taxis/{serial}', ['uses' => 'Boxes@taxis']);
    Route::put('channel/{serial}', ['uses' => 'Boxes@lastChannel']);
    Route::post('sendvideo/{serial}', ['uses' => 'Boxes@sendVideo']);
    Route::post('reset/{serial}', ['uses' => 'Boxes@reset']);
    Route::post('box', ['uses' => 'Boxes@postBox']);
    Route::put('box/{id}', ['uses' => 'Boxes@putBox']);
    Route::post('rating/{serial}', ['uses' => 'Boxes@videoRating']);
    Route::post('state/{serial}', ['uses' => 'Boxes@stateNoEvent']);
    Route::get('getvideo/{serial}', ['uses' => 'Boxes@getVideo']);
    Route::post('damagecontrol/{serial}', ['uses' => 'Boxes@damageControl']);
    Route::get('channels', ['uses' => 'Boxes@getChannels']);
    Route::post('channels', ['uses' => 'Boxes@postChannel']);
    Route::post('channels/{id}', ['uses' => 'Boxes@putChannel']);
//    ->middleware('auth:api');
});

Route::group(['prefix'=>'logs'], function(){
    Route::post('{serial}', ['uses' => 'Logs@state']);
//    ->middleware('auth:api');
});

Route::group(['prefix'=>'socket'], function(){
    Route::post('{event}/{serial}', ['uses' => 'Socket@emit']);
//    ->middleware('auth:api');
});

Route::group(['prefix'=>'statistics'], function(){
    //TYPE CAN BE NOTIFIED,INJECTED,FORCED
    //EVENT CAN BE NOTIFICATION, LIBRARY
    Route::get('asgie/created', ['uses' => 'Statistics@createdVideosAsgie']);
    Route::get('asgie/unseen', ['uses' => 'Statistics@unseenVideosAsgie']);
    Route::get('asgie/seen', ['uses' => 'Statistics@seenVideosAsgie']);
    Route::get('videos/box/unseen', ['uses' => 'Statistics@unseenvideosBox']);
    Route::get('videos/box/seen', ['uses' => 'Statistics@seenVideosBox']);
    Route::get('videos/box/rewatched', ['uses' => 'Statistics@rewatchedVideosBoxLibrary']);
    Route::get('polly', ['uses' => 'Statistics@pollyChars']);
    Route::get('videos', ['uses' => 'Statistics@videos']);
});

Route::group(['prefix'=>'recommendations'], function(){
    Route::get('videos', ['uses' => 'Recommendations@videos']);
    Route::get('ratings', ['uses' => 'Recommendations@ratings']);
    Route::get('users', ['uses' => 'Recommendations@users']);
});

//Route::group(['prefix'=>'recommendations2'], function(){
//    Route::get('videos', ['uses' => 'Recommendations@videos2']);
//    Route::get('ratings', ['uses' => 'Recommendations@ratings2']);
//    Route::get('users', ['uses' => 'Recommendations@users2']);
//    Route::get('all', ['uses' => 'Recommendations@all']);
//});

Route::group(['prefix'=>'mobile'], function(){
    Route::get('videos/{serial}', ['uses' => 'Boxes@appVideos']);
    Route::get('recommendations/{serial}', ['uses' => 'Boxes@appRecommended']);
    Route::get('pushvideotv/{serial}', ['uses' => 'Boxes@sendVideoId']);
    Route::get('seen/{serial}', ['uses' => 'Boxes@seenMobile']);
});

Route::group(['prefix'=>'sendKey'], function(){
    Route::post('{serial}', ['uses' => 'Boxes@sendKey']);
});


// Testing route DELETE AFTER TESTING
//Route::group(['prefix'=>'testing'], function(){
//    Route::get('{id}', ['uses' => 'Testing@superTesting']);
//});
//
//Route::group(['prefix'=>'testing2'], function(){
//    Route::get('', ['uses' => 'Testing@megaTesting']);
//});