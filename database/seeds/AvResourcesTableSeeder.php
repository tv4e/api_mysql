<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\AvResourcesType;

class AvResourcesTableSeeder extends Seeder
{
    public function run()
    {
        $files = Storage::cloud()->allFiles(env('CDN_IMAGE'));

        foreach ($files as $file){
            DB::table('av_resources')->insert([
                'url' => env('CDN') .'/'.$file,
                'av_resources_type_id' => AvResourcesType::where('type','image')->first()->id
            ]);
        }

        $files = Storage::cloud()->allFiles(env('CDN_VIDEO'));

        foreach ($files as $file){
            DB::table('av_resources')->insert([
                'url' => env('CDN') .'/'.$file,
                'av_resources_type_id' => AvResourcesType::where('type','video')->first()->id
            ]);
        }

        $files = Storage::cloud()->allFiles(env('CDN_AUDIO'));

        foreach ($files as $file){
            DB::table('av_resources')->insert([
                'url' => env('CDN') .'/'.$file,
                'av_resources_type_id' => AvResourcesType::where('type','audio')->first()->id
            ]);
        }

    }
}
