<?php

use Illuminate\Database\Seeder;

class AvResourcesTypesTableSeeder extends Seeder
{
    public function run()
    {
        $types=[
            'video',
            'video-intro',
            'video-outro',
            'audio',
            'image'
        ];

        foreach ($types as $type){
            DB::table('av_resources_types')->insert([
                'type' => $type
            ]);
        }
    }
}
