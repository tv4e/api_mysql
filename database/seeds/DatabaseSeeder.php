<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AvResourcesTypesTableSeeder::class);
        $this->call(AvResourcesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(AsgieTableSeeder::class);
        $this->call(FiltersTableSeeder::class);
        $this->call(InformationSourcesTableSeeder::class);
    }
}
