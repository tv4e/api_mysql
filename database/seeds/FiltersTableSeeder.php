<?php

use Illuminate\Database\Seeder;
use App\Filter;

class FiltersTableSeeder extends Seeder
{
    public function run()
    {
        $filters=[
            'sénior',
            'seniores',
            'idoso',
            'idosa',
            'terceira idade',
            'envelhecer',
            'envelhecimento',
            'ancião',
            'centenário',
            'velho',
            'lar de idosos',
            'reforma',
            'pensão',
            'pensões',
            'veterano',
            'avó',
            'avô',
            'invalidez',
            'demência',
            'passeio',
            'evento',
        ];

        foreach ($filters as $filter){
            Filter::create([
                'filter' => $filter
            ]);
        }
    }
}
