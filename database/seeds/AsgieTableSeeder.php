<?php

use Illuminate\Database\Seeder;
use App\Asgie;

class AsgieTableSeeder extends Seeder
{
    public function run()
    {
        $asgies=[
            'Health Care and Welfare Services',
            'Social Services',
            'Financial Services',
            'Culture, Informal Education and Entertainment',
            'Security Services',
            'Local Authority Services',
            'Transport Services'
        ];

        foreach ($asgies as $asgie){
            Asgie::create([
                'title' => $asgie
            ]);
        }

    }
}
