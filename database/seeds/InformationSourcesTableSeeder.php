<?php

use Illuminate\Database\Seeder;
use App\InformationSource;

class InformationSourcesTableSeeder extends Seeder
{
    public function run()
    {
        $sources = [
            [
                'url' => 'https://www.sns.gov.pt/noticias/',
                'news_container' => 'div.sns-container',
                'news_link' => 'div.cf > div.post-short-wrapper > div.text-wrap > h2.post-title > a',
                'content_container' => 'article.content',
                'content_title' => 'div.post-info-bar > h1.page-title',
                'content_description' => 'div.row > div > p',
                'asgie_id' => '1',
                'city_id' => null,
            ],
            [
                'url' => 'http://www.publico.pt',
                'news_container' => 'div.group.content-main > section.primary',
                'news_link' => 'article > header.entry-header > a',
                'content_container' => 'article.story',
                'content_title' => 'header.story__header > h1.headline',
                'content_description' => 'div.story__content > div.story__body > p',
                'asgie_id' => '3',
                'city_id' => null,
                'information_sources_subs'=>[
                    [
                        'url' =>  '/saude',
                        'city_id' => '/category/por-terras-da-bairrada/anadia/',
                        'asgie_id' => '/category/por-terras-da-bairrada/anadia/',
                    ]
                ]

            ],
            [
                'url' => 'http://www.cartasocial.pt/',
                'news_container' => 'td#corpo2as',
                'news_link' => 'table > body > tr > td > table > tbody > tr > td > a.pesq',
                'content_container' => 'td#corpo2as',
                'content_title' => 'table > tbody > tr > td > div.cabTabPesq7',
                'content_description' => 'table > tbody > tr > td.info2',
                'asgie_id' => '2',
                'city_id' => null
            ],
            [
                'url' => 'http://www.jornaldenegocios.pt/economia/impostos',
                'news_container' => 'div#firstblockNoticias',
                'news_link' => 'div > div > div.noticiaDestaque > div.col_left > a.h2',
                'content_container' => 'div.alturaNoticia',
                'content_title' => 'div#blocoNoticia > div.noticiaTitle > h1',
                'content_description' => 'div#blocoNoticia > div.newsContent > div',
                'asgie_id' => '3',
                'city_id' => null
            ],
            [
                'url' => 'http://www.dn.pt/tag/turismo.html',
                'news_container' => 'main.t-main-3 > div.t-main-inner > section.t-section-1 > div.t-section-inner > div.t-modular-sys-C3.t-modular-sys-GPC3L08.js-modular-sys-1.t-reveal',
                'news_link' => 'div.t-modsys-item.h3 > article.t-modular-card-1.t-module-C3_A003 > header > a.t-article-link-2',
                'content_container' => 'main.t-main-2 > article.t-article-1.js-select-and-share-1.js-readmore-article-1',
                'content_title' => 'header > h1.t-article-title-1.selectionShareable ',
                'content_description' => 'div.t-article-wrap-1.clfx t-readmore-article-1 > div.t-article-wrap-inner > div.t-article-inner-inner > div.t-article-content-2 > p.selectionShareable ',
                'asgie_id' => '4',
                'city_id' => null
            ],
            [
                'url' => 'http://jb.pt',
                'news_container' => 'div#content-area',
                'news_link' => 'h2.post-title.entry-title>a',
                'content_container' => 'div#main-content>div#container>div#content-area>div.et_pb_extra_column_main>article',
                'content_title' => 'div.post-header>h1',
                'content_description' => 'div.post-wrap>div.post-content.entry-content>p',
                'asgie_id' => '4',
                'city_id' => null,
                'information_sources_subs'=>[
                    [
                        'url' => '/category/por-terras-da-bairrada/anadia/',
                        'city_id' => '/category/por-terras-da-bairrada/anadia/',
                        'asgie_id' => '/category/por-terras-da-bairrada/anadia/',
                    ],
                    [
                        'url' => '/?s=eventos',
                        'city_id' => null,
                        'asgie_id' => null
                    ]
                ]
            ],
            [
                'url' => 'http://www.diarioaveiro.pt',
                'news_container' => 'div#div_content>row',
                'news_link' => 'div.col-md-4>row>a.newslink.news_title',
                'content_container' => 'div#main>div.row>div.col-md-9>div.row',
                'content_title' => 'div.col-md-12>h1.page_title',
                'content_description' => 'div.col-md-12.news_resume',
                'asgie_id' => '4',
                'city_id' => null,
                'information_sources_subs'=>[
                    [
                        'url' => '/pesquisa/cultura',
                        'city_id' => null,
                        'asgie_id' => null
                    ]
                ]
            ],
            [
                'url' => 'http://www.jn.pt',
                'news_container' => 'section.t-grid-1 > div.t-grid-inner > div.t-grid-inner-inner',
                'news_link' => 'article.t-g1-l4-am1 > header > h2 > a',
                'content_container' => 'article.t-article-1.js-readmore-article-1.js-readmore-article-created',
                'content_title' => 'header > h1',
                'content_description' => 'div.t-article-content.t-readmore-article-1 > div.t-article-content-inner,js-select-and-share-1> p.selectionShareable',
                'asgie_id' => '5',
                'city_id' => null,
                'information_sources_subs'=>[
                    [
                        'url' => '/tag/gnr.html',
                        'city_id' => null,
                        'asgie_id' => null
                    ],
                    [
                        'url' => '/tag/psp.html',
                        'city_id' => null,
                        'asgie_id' => null
                    ],
                    [
                        'url' => '/tag/policia.html',
                        'city_id' => null,
                        'asgie_id' => null
                    ]
                ]
            ],
            [
                'url' => 'http://www.cm-aveiro.pt',
                'news_container' => 'div.MainCenterArea > div#MainArea > div.DivOtherNews',
                'news_link' => 'div.OtherNew > div.TextOtherNew > div.TitleOtherNew > a',
                'content_container' => 'div.MainCenterArea > div#MainArea > div.NewsBreafWrapper > div.Noticia',
                'content_title' => 'div.NoticiaHeader > div.NoticiaSubHeader > div.NoticiaSubHeaderTitle > span',
                'content_description' => 'div.NoticiaBodyDetail > p > span > span',
                'asgie_id' => '5',
                'city_id' => null
            ],
        ];

        foreach ($sources as $source){
            $sourceId = DB::table('information_sources')->insertGetId([
                'url' => $source['url'],
                'news_container' => $source['news_container'],
                'news_link' => $source['news_link'],
                'content_container' => $source['content_container'],
                'content_title' => $source['content_title'],
                'content_description' => $source['content_description'],
                'asgie_id' => $source['asgie_id'],
                'city_id' => $source['city_id'],
            ]);

            if(isset($source['information_sources_subs'])){
                foreach ($source['information_sources_subs'] as $sub) {
                    DB::table('information_sources_subs')->insert([
                        'url' => $sub['url'],
                        'information_source_id' => $sourceId
                    ]);
                }
            }
        }
    }
}
