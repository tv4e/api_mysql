<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformativeVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informative_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data', 10000);
            $table->string('title', 500);
            $table->integer('information_source_id')->unsigned()->index();;
            $table->foreign('information_source_id')->references('id')->on('information_sources');
            $table->integer('av_resource_id')->unsigned()->index();;
            $table->foreign('av_resource_id')->references('id')->on('av_resources');
            $table->datetime('deleted_at')->nullable();
            $table->datetime('expired_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informative_videos');
    }
}
