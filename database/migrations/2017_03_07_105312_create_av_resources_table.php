<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('av_resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 500)->unique();
            $table->string('name', 250)->nullable()	;
            $table->integer('av_resources_type_id')->unsigned()->index();
            $table->foreign('av_resources_type_id')->references('id')->on('av_resources_types')->onDelete('cascade');
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('av_resources');
    }
}
