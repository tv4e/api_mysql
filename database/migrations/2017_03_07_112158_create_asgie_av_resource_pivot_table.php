<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsgieAvResourcePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asgie_av_resource', function (Blueprint $table) {
            $table->integer('asgie_id')->unsigned()->index();
            $table->foreign('asgie_id')->references('id')->on('asgie')->onDelete('cascade');
            $table->integer('av_resource_id')->unsigned()->index();
            $table->foreign('av_resource_id')->references('id')->on('av_resources')->onDelete('cascade');
            $table->primary(['asgie_id', 'av_resource_id']);
            $table->boolean('selected');
            $table->integer('duration_percentage')->nullable();
            $table->datetime('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asgie_av_resource');
    }
}
