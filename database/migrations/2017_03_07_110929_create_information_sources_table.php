<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 500)->unique();
            $table->string('news_container', 500);
            $table->string('news_link', 500);
            $table->string('content_container', 500);
            $table->string('content_title', 500);
            $table->string('content_description', 500);
            $table->integer('asgie_id')->unsigned()->index();;
            $table->foreign('asgie_id')->references('id')->on('asgie');
            $table->unsignedInteger('city_id')->nullable()->unsigned()->index();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('information_sources');
    }
}
