<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationSourcesSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_sources_subs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 1000);
            $table->unsignedInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->integer('information_source_id')->unsigned()->index();
            $table->foreign('information_source_id')->references('id')->on('information_sources')->onDelete('cascade');
            $table->integer('carta_social_id')->unsigned()->index();
            $table->foreign('carta_social_id')->references('id')->on('carta_social')->onDelete('cascade');
            $table->boolean('content');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('information_sources_subs');
    }
}
