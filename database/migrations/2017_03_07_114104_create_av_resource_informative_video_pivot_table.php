<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvResourceInformativeVideoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('av_resource_informative_video', function (Blueprint $table) {
            $table->integer('av_resource_id')->unsigned()->index();
            $table->foreign('av_resource_id')->references('id')->on('av_resources')->onDelete('cascade');
            $table->integer('informative_video_id')->unsigned()->index();
            $table->foreign('informative_video_id')->references('id')->on('informative_videos')->onDelete('cascade');
            $table->primary(['av_resource_id', 'informative_video_id'],'smh_id');
            $table->datetime('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('av_resource_informative_video');
    }
}
