<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxInformativeVideoPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_informative_video', function (Blueprint $table) {
            $table->integer('box_id')->unsigned()->index();
            $table->foreign('box_id')->references('id')->on('boxes')->onDelete('cascade');
            $table->integer('informative_video_id')->unsigned()->index();
            $table->foreign('informative_video_id')->references('id')->on('informative_videos')->onDelete('cascade');
            $table->primary(['box_id', 'informative_video_id']);
            $table->boolean('seen');
            $table->boolean('rejected');
            $table->datetime('deleted_at')->nullable();
            $table->datetime('sent_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('box_informative_video');
    }
}
