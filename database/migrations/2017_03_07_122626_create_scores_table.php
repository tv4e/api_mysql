<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->integer('box_id')->unsigned()->index();
            $table->foreign('box_id')->references('id')->on('boxes')->onDelete('cascade');
            $table->integer('asgie_id')->unsigned()->index();
            $table->foreign('asgie_id')->references('id')->on('asgie')->onDelete('cascade');
            $table->integer('score');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->primary(['box_id', 'asgie_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scores');
    }
}
